function I = BPRecon(RHS, Setup)
Stage.N_steps = 1*(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
Geo.R0 = Geo.Det.Rd-Geo.R;
d0 = 2*Setup.R/(Stage.N_steps); % effective resolution. It is also Data.ca/(2 N_f delta_f)
f0 = Setup.FreqSpec(1);
pl = exp(-1j*2*pi*f0*(0:Stage.N_steps-1)*d0/Setup.ca);
for k = 1 : Setup.DetNumber
    ql(k,:) = exp(1j*(Geo.R0(k)+d0)*Setup.w/Setup.ca);%*exp(-1j*2*pi*f0*Geo.d0/Data.ca(2));
    Rn(k,:) = Setup.DetRad(k)+(1:Stage.N_steps)*d0;%-Geo.d0/2;
end

M = zeros(Data.N_f,Geo.N_steps);
for m = 1 : Data.N_f
    M(m,:) = exp(-1j*2*pi*(m-1)*Data.alpha*[0:Geo.N_steps-1]/(Geo.N_steps));
end
Data.pl = pl;
M = M*diag(pl);
Recon.M = M;

%%
Q = reshape(-1j*conj(Data.P), Geo.Det.Number, Data.N_f);
Q = Q ./(ones(Geo.Det.Number,1)*Data.w);
Q = Q .* ql;
if 1
    if Data.alpha == 1
        q2 = 1;
    else
        q2 = 10;
    end
    q1 = round(Data.alpha*q2);
    Recon.qs = [q1 q2];
    Tem = zeros(Geo.Det.Number,q2*Geo.N_steps);
    Tem(:,1:q1:q1*(Data.N_f-1)+1) = Q; % For phase only do ./abs(Q)
    g0 = ifft(Tem,[],2);
    Dat = 2*real(g0(:,1:Geo.N_steps)*diag(1./pl))*q2*Geo.N_steps;

end
Dat = max(Dat,0).*Rn/Geo.Det.Number;
Dat = Dat*(Geo.Resolution(1)/Geo.Resolution(2))^2;
%%
Recon.Dat = Dat;
Datp = Dat';
if Geo.Det.Number == 1
    solution = Datp(Geo.Det.Mapping);
else
    solution = sum(Datp(Geo.Det.Mapping),1);
end
I = NaN(size(Geo.Xi));
sig = solution;
I(Geo.indi) = sig;
I3 = I;
I(isnan(I)) = mean(I(~isnan(I)));
F = fft2(I);
if Geo.Det.Number > 1
    F = F(:).*Geo.LL(:);
end
K = (max(0,real(ifft2(reshape(F,size(Geo.Xi,1),size(Geo.Xi,1))))));
%%
K(isnan(I3)) = NaN;
if ~NN_FLAG
    Recon.I_FFT = K;
else
    Recon.I_NN = K;
end
return
%% Plots for debugging
if 0
    figure
    t = SH;
    subplot(311)
    plot(Vec(:,t));hold on;plot(Mec*Dat(t,:)','r-');
    subplot(312)
    plot(abs(Q(t,:)));hold on;plot(abs(M*Dat(t,:)'),'r-');
    subplot(313)
    plot(Dat(t,:));
end

end

