    Objects = [+1e-3 -0.5e-3 0.5e-3 1;
        -1e-3 0.5e-3 0.5e-3 1;
      %  +0.000+Setup.Resolution(2)*0 -0.003 .0001 1;
        %+0.000 -0.003 .0003 1;
        %0.005 -0.004 .0006 1.5;
     %   -Setup.R*0.69 +Setup.R*0.69 .0002 1;
        %+0.000 0.000 .0003 1;
        %-Setup.R*0.56 +Setup.R*0.69 .0002 1;
        ]; % Soviele Objekte wie du willst: [position_x position_y radius absorption]
Mu = ones(numel(Geo.Xf),1)*0.0;

%OBJECTS = [];
for k = 1 : size(Objects,1)
    P0 = Objects(k,[1 2]);
    if k == -1
        ind0 = find((Geo.Xf-P0(1)).^2+(Geo.Yf-P0(2)).^2 < Objects(k,3).^2 & (Geo.Xf-P0(1)).^2+(Geo.Yf-P0(2)).^2 > (Objects(k,3)*0.7).^2);
    else
        ind0 = find((Geo.Xf-P0(1)).^2+(Geo.Yf-P0(2)).^2 < Objects(k,3).^2);
    end
    Mu(ind0) = Objects(k,4);
end
d0 = Geo.R*0.9;
Mu = Mu(Geo.indf);
Data.P_nonoise = Recon.W0f*Mu;
Data.noise = randn(size(Data.P_nonoise))+1j*randn(size(Data.P_nonoise));
Data.coef = (10^(-Recon.SNR/20)*norm(Data.P_nonoise))/norm(Data.noise);
Data.P = Data.P_nonoise+Data.coef*Data.noise;
%snr(Data.P, Data.P_nonoise)
I3 = 0*Geo.Xf;
I3(Geo.indf) = Mu;
Recon.Mu = Mu;
Recon.TrueImage = I3;
figure(31);h31 = imagesc(I3);axis equal;axis tight