%%  SIMULATION PART
clear all
Data.BW = 3e6;
Data.ca = [1500 1500]; % actual and assumed values
Geo.R = 0.005;% Reconstruction radius. Half of the side length of the square in the middle

Data.delta_f = 1*Data.ca(2)/(2*Geo.R);

Data.N_f = round(Data.BW/Data.delta_f)+1;%Data.ca(1)/(2*Data.BW)
Data.F0 = 0.1e6+[0:Data.N_f-1]*Data.delta_f;
Geo.Resolution = [30e-6 50e-6]; % Forward and inverse resolution
Data.BW = max(Data.F0)-min(Data.F0);
Data.w = 2*pi*Data.F0;
Data.N_f = length(Data.w);
Data.alpha = (2*Geo.R*Data.delta_f)/Data.ca(2);

Recon.Used_Fs = 1:Data.N_f; % This one would only work for experimental data
%% Building detector array
Geo.Det.Array_Mode = 'circular';
switch Geo.Det.Array_Mode
    case 'circular'
        Geo.Det.Rad0 = [0.007 0.007]; % [actual and assumed]%Geo.Len^2/Geo.Resolution;%sqrt(2)*Geo.Len*5; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
        Angular_error = abs(diff(Geo.Det.Rad0))*2*max(Data.F0)*180/(Data.ca(1));
        Geo.Det.Number = 10;
        Geo.Det.Angles = [0:Geo.Det.Number-1]*360/Geo.Det.Number;% linspace(0,360-1,Geo.Det.Number);
        theta = Geo.Det.Angles*pi/180;
        Detf = [Geo.Det.Rad0(1)*cos(theta);Geo.Det.Rad0(1)*sin(theta)];
        Deti = [Geo.Det.Rad0(2)*cos(theta);Geo.Det.Rad0(2)*sin(theta)];
        Deti1 = [Geo.Det.Rad0(1)*cos(theta);Geo.Det.Rad0(1)*sin(theta)];
    case 'planar'
        Geo.Det.Rad0 = [0.013 0.013];
        Geo.Det.Span = .01;
        Geo.Det.Number = 16;
        Det = [linspace(-0.5*Geo.Det.Span,0.5*Geo.Det.Span,Geo.Det.Number);ones(1,Geo.Det.Number)*Geo.Det.Rad0(1)];
        Detf = [-Det([2 1],:) Det([2 1],:) -Det];
        Det = [ones(1,Geo.Det.Number)*Geo.Det.Rad0(2);linspace(-0.5*Geo.Det.Span,0.5*Geo.Det.Span,Geo.Det.Number)];
        Deti = [-Det([2 1],:) Det([2 1],:) -Det];
        Deti1 = Deti;
        
        %Det = [Det -Det];
    case 'arcs'
        Geo.Det.Rad0 = 0.03;%Geo.Len^2/Geo.Resolution;%sqrt(2)*Geo.Len*5; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
        Geo.Det.Number = 64;
        Geo.Det.Angles = linspace(50,130,Geo.Det.Number);
        theta = Geo.Det.Angles*pi/180;
        Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
        Det = [Geo.Det.Rad0*cos(theta);1*Geo.Det.Rad0*sin(theta)-0.015];
        Det = [Det -Det];
end
Geo.Det.Number = size(Detf,2);
Geo.Det.Positionsf = Detf;%clear Det;
Geo.Det.Positionsi = Deti;%clear Det;
Geo.Det.Positionsi1 = Deti1;
%% Building the weight matrix
Build_weighmatrix
%% Plot geometry
figure
subplot(224)
plot(Geo.Xf(Geo.indf),Geo.Yf(Geo.indf),'r.');hold on;axis off
plot(Geo.Det.Positionsi(1,:),Geo.Det.Positionsi(2,:),'bs') % because here inverse means assumed
axis equal;
%% Forward simulation
Recon.SNR = Inf; % The intended SNR
Forward_Simulation;if exist('h31') & ishandle(h31) close(31); end
%% Die Messungen im experimentellen Format
A = reshape(Data.P,Geo.Det.Number,Data.N_f);

%% Inverting using kspace and model-based
tic; [Recon, Geo] = Model_based_fd(Recon, Geo, Data,0);toc % if the last flag set to 1, it would refine the intial radius guess
% USE FFTBP
tic; [Recon, Data,Geo] = K_space_fd_gen(Recon, Geo, Data,0); toc
subplot(222)
Display_results(gca,Recon.I_FFT, Geo, Data);title('FFT')
colorbar
subplot(223)
h = imagesc(Recon.TrueImage);set(h,'AlphaData',Data.Maskf);axis equal;axis tight
maximize
subplot(221)
if isfield(Recon,'I_mb')
    Display_results(gca,Recon.I_mb,Geo, Data);
end
%Angular_speed = abs(diff(Data.ca))*max(Data.w)*mean(Geo.r_rdi1(:))*180/(pi*Data.ca(1)^2);
return