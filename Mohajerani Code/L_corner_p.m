function [NorX, NorY] = L_corner_p(Weight, Recon, RHS, FLAG)
NorY = [];
NorX = [];


if FLAG == 0
    return
end
for k = 1 : size(Recon.recon,2)
    v = Recon.reconneg(:,k);
    result = Weight*v-RHS;
    NorX(k) = norm(result);
    NorY(k) = norm(v);
end
