function I = Display_results(ax, x, Geo, Data, in, Recon)
%axes(ax)
%opengl software

if nargin == 6
    recon = Recon.recon;
    x = (recon.recon(:,in));
    I = 0*Geo.X;
    I(Geo.ind) = x;
else
    
    if length(size(x)) == 1
        I = 0*Geo.X;
        I(:) = x;
    else
        I = x;
    end
end
%I = I .* imerode(I0 > 0,strel('disk',1)); % Minimual eroding to get rid of boundary artifacts
%I0 = I0 .* imerode(I0 > 0,strel('disk',1)); % Minimual eroding to get rid of boundary artifacts
h = imagesc(I);%[-Geo.R Geo.R]*1000,[-Geo.R Geo.R]*1000,
set(h,'AlphaData',Data.Mask);
%colorbar
hold on
axis equal;
axis tight;
xlabel('mm')
ylabel('mm')
