function s1 = diflam(s, L)
s1(1) = (s(2)-s(1))/(L(2)-L(1));
s1(length(L)) = (s(length(L))-s(length(L)-1))/(L(length(L))-L(length(L)-1));
for k = 2 : length(L)-1
    s1(k) = mean([(s(k+1)-s(k))/(L(k+1)-L(k)) (s(k-1)-s(k))/(L(k-1)-L(k))]);
end

