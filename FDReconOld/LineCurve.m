function in = LineCurve(Recon, PLOT)
if PLOT == 0
    in = 90;
    return
end
L_Curve = Recon.L_Curve;
x = L_Curve.NorResidue;
y = L_Curve.NorVector;
% kr = LineCurvature2D([x; y]');
% kr(1:50) = 0;
% [ig, in] = max(kr);
% K = in;
% %return
% plot(L_Curve.NorResidue,L_Curve.NorVector/max(L_Curve.NorVector),'k:');hold on;
% plot(L_Curve.NorResidue(K),L_Curve.NorVector(K)/max(L_Curve.NorVector),'kd')
% K1 = 145;plot(L_Curve.NorResidue(K1),L_Curve.NorVector(K1)/max(L_Curve.NorVector),'bd')
% K1 = 130;plot(L_Curve.NorResidue(K1),L_Curve.NorVector(K1)/max(L_Curve.NorVector),'rd')
%
% plot(L_Curve.NorResidue(K),kr(K)/max(kr),'kd')
% K1 = 145;plot(L_Curve.NorResidue(K1),kr(K1)/max(kr),'bd')
% K1 = 130;plot(L_Curve.NorResidue(K1),kr(K1)/max(kr),'rd')
%
% plot(L_Curve.NorResidue,kr/max(kr),'r-')
% ylim([0 1.05])
% axis tight
%
% [~,K] = min(sqrt((x-min(x)).^2+((y-min(y))/max(y)).^2));
% plot(L_Curve.NorResidue(K),L_Curve.NorVector(K)/max(L_Curve.NorVector),'g*')
% plot(L_Curve.NorResidue,L_Curve.NorVector/max(L_Curve.NorVector),'r.')
% return
%% my own curvature def.
if 1
    %%
    L = (Recon.lambdas);
    s = (x.^1);
    n = (y.^1);
    %[a,b,c] = l_corner(s',n',L');
    %find(L == a)
    %%%
    s1 = diflam(s, L);
    s2 = diflam(s1, L);
    n1 = diflam(n, L);
    n2 = diflam(n1, L);
    kr = abs(s1.*n2-s2.*n1)./(s1.^2+n1.^2+10).^(3/2); % from the book
    %krl = (n.*s./n1).*(L.^2.*n1.*s+2*L.*n.*s+L.^4.*n.*n1)./(s.^2.*L.^2+s.^2).^(3/2); % from http://www2.imm.dtu.dk/documents/ftp/tr99/tr15_99.pdf
    kr(1:50) = kr(50);
    [ig, in] = max(kr);
    K = in;
    if 0
        plot(L_Curve.NorResidue,L_Curve.NorVector/max(L_Curve.NorVector),'k:');hold on;
        plot(L_Curve.NorResidue(K),L_Curve.NorVector(K)/max(L_Curve.NorVector),'kd')
        %K1 = 145;plot(L_Curve.NorResidue(K1),L_Curve.NorVector(K1)/max(L_Curve.NorVector),'bd')
        %K1 = 130;plot(L_Curve.NorResidue(K1),L_Curve.NorVector(K1)/max(L_Curve.NorVector),'rd')
        
        plot(L_Curve.NorResidue(K),kr(K)/max(kr),'kd')
        %K1 = 145;plot(L_Curve.NorResidue(K1),kr(K1)/max(kr),'bd')
        %K1 = 130;plot(L_Curve.NorResidue(K1),kr(K1)/max(kr),'rd')
        
        plot(L_Curve.NorResidue,kr/max(kr),'r-')
        ylim([0 1.05])
        axis tight
    end
    in = in;
else
    %% closes to origin
    L_Curve = Recon.L_Curve;
    x = (L_Curve.NorResidue);
    y = (L_Curve.NorVector);
    
    L = (Recon.lambdas);
    [~,K] = min(sqrt(((x-min(x))/max(x)).^2+((y-min(y))/max(y)).^2));
    in = K;
    %return
    if PLOT == 1
        plot(L_Curve.NorResidue,L_Curve.NorVector/max(L_Curve.NorVector),'k:');hold on;
        plot(L_Curve.NorResidue(K),L_Curve.NorVector(K)/max(L_Curve.NorVector),'kd')
        K1 = 145;plot(L_Curve.NorResidue(K1),L_Curve.NorVector(K1)/max(L_Curve.NorVector),'bd')
        K1 = 130;plot(L_Curve.NorResidue(K1),L_Curve.NorVector(K1)/max(L_Curve.NorVector),'rd')
        
        ylim([0 1.05])
        axis tight
        title(in)
    end
end
%axis tight
%xlim([L_Curve.NorResidue(20) L_Curve.NorResidue(in+10)]);
%in = min(145,in);
%in = min(in, in1);