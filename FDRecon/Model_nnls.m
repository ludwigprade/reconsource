%performing NNLS with the projected conjugate gradient method
%r = b-A*x;
%Input: x--initial guess of the solution (usually set as vector 0)
%       zetaRel--relative infinity norm of the projected gradient,used as stopping criteria (e.g. 0.01)
%       nIter--max. number of outer iteration
%       cgIter--max. number of inner iteration (usually set below 5, depending on the problem size)
%Output: x--solution
%        f0--vector containing the value ||Ax-b||^2 after each outer iteration
%        t-- vector containing the execution time after each outer iteration
%terminates if nIter or zetaRel is reached or t>10 min.

function [Recon, Geo] = Model_nnls(Recon, Geo, Data)
tic;
Radii = Geo.Det.Rad0; % Scannig and optimizing over 4 mm
% Building the weight matrix
% inversion
Weight.W = [real(Recon.W0);imag(Recon.W0)];
RHS = [real(Data.P);imag(Data.P)];
Weight.n_voxels = Geo.N_vox;
recon.lambdas = getlambdas(Weight.W);
recon.Linv = speye(Weight.n_voxels,Weight.n_voxels); 
disp('Starting nls-lsqr');
[x, f0, t] = nnls_conjgrad_armijo(Weight.W, RHS, zeros(Geo.N_vox,1), 0.001, 50, 5); 
disp('Finishing lsqr');
Recon.xmb = x;
Recon.Radii=Radii;
I = 0*Geo.X;
I(Geo.ind) = x;
Recon.I_mb = I;
Recon.f0=f0;
Recon.t=t;
clear Weight
clear RHS
toc