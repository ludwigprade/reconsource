timeStep = 0.001;
time = 0:timeStep:(1-timeStep);

amplitude1 = 0.2;
frequency1 = 10;
amplitude2 = 0.8;
frequency2 = 14;
amplitude3 = 0.6;
frequency3 = 18;

waveform1 = amplitude1*sin(2*pi*frequency1*time);
waveform2 = amplitude2*sin(2*pi*frequency2*time);
waveform3 = amplitude3*sin(2*pi*frequency3*time);

waveform = waveform1 + waveform2 + waveform3;
waveform = waveform + 0.3*rand(1,size(waveform,2));

waveformArray = (waveform./max(waveform))';
plot(waveformArray);
xlabel('Samples');
ylabel('Amplitude');

f = fgen;
f.Resource = 'USB0::0x09c4::0x0400::DG1D154402286::INSTR';
connect(f);
selectChannel(f, '1');
f.Waveform = 'Arb';
downloadWaveform (f, waveformArray);
enableOutput(f);
clear f;