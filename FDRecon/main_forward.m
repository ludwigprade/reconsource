warning off
%% geometrical settings
clear all
close all
load NFF
clc
%clc

%% discretization of imaging space
LFLAG = 0; % if == 1 then L curve and curvature are found (takes max less than 30 seconds). If 0: then a preset lambda is used, much faster and is also reliable in most cases.
L = 20e-6; % Voxel side
NoiseMultiplier = 10000; % changing this changes the SNR

R = 3e-3;% Sample radius
[X, Y] = meshgrid([-R:L:R],[-R:L:R]);
ind = find(X.^2+Y.^2 < R.^2); % Number of voxels in sample

%% frequencies
fstart = 0.5;
fend = 5;
% fstep=0.5;
fn=21;
w=2*pi*1e6*linspace(fstart,fend,fn);
%w = 2*pi*(fstart:fstep:fend)*1e6;

N_f = length(w);

%% geometry parameters
ca = 1480;
N_ang = 180;
W0 = zeros(N_ang*N_f,length(ind));
Angles = linspace(0,360,N_ang);
N_vox = length(ind);

theta = Angles*pi/180;
rd = R*exp(j*theta);

r_rd = sqrt( ...
    ((X(ind)*ones(1,N_ang))' - real(rd)'*ones(1,N_vox)).^2 + ...
    ((Y(ind)*ones(1,N_ang))' - imag(rd)'*ones(1,N_vox)).^2);
for f = 1 : N_f
    u = exp((j*(w(f)/ca)*r_rd))./r_rd;
    W0([1:N_ang]+(f-1)*N_ang,:) = u;     
end

%% Simulation : forward
%% new
Phi_A = 0;
% Objects: as many as you want with different size and absorption
%pos_x, pos_y, r, absorbance
Objects = [
    0.2e-3 +0.1e-3 0.25e-3 1;
    1.58e-3 -0.88e-3 0.25e-3 1;
    %0.001 -.001 .0005 1;
    %-0.001 -.001 .0008 1;
    %+.001 +.001 .00002 1;
    %+.0 +.0015 .00025 1;
    ]; % many objects with different size
Mu = zeros(numel(X),1);

for k = 1 : size(Objects,1)
    P0 = Objects(k,[1 2]);
    ind0 = find((X-P0(1)).^2+(Y-P0(2)).^2 < Objects(k,3).^2);
    Mu(ind0) = Objects(k,4);
end

%% absorption: forward
Mu = Mu(ind);
%W = exp(j*Phi_A)*W0;
P11 = W0*Mu;
%% add noise
 noise = (randn(length(P11),1)+randn(length(P11),1)*i)*NoiseMultiplier*norm(P11,'fro');
 P = P11+ noise;

% do the inversion
main_inverse_lsqr

% p_out=zeros(21,180);
% for f=1:21
%    for phi=1:180
%        p_out(f,phi)=P11(f*phi);
%    end
% end