%% Initializations
if ~isfield(Geo,'X') % The very first run
    Geo.N_steps = 1*(round(Geo.R*2/Geo.Resolution));
    [Geo.X, Geo.Y] = meshgrid(linspace(-Geo.R, Geo.R,Geo.N_steps+1),linspace(-Geo.R, Geo.R,Geo.N_steps+1));
    Geo.ind = find(Geo.X.^2+Geo.Y.^2 < Geo.R.^2);
    Data.BW = max(Data.F0)-min(Data.F0);
    Data.w = 2*pi*Data.F0;
    Data.N_f = length(Data.w);
   % Data.alpha = (2*Geo.R*Data.delta_f)/Data.ca;
    Geo.N_vox = numel(Geo.ind);
    Recon.W0 = zeros(Geo.Det.Number*Data.N_f,Geo.N_vox);
    I1 = 0*Geo.X;I1(Geo.ind) = 1;Data.Mask = I1;
end

%% Building the weight matrix

Geo.Det.Number = size(Data.p,1);
Geo.Det.Angles = linspace(0,359,Geo.Det.Number);
theta = Geo.Det.Angles*pi/180;
Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
Geo.Det.Number = size(Det,2);
Geo.Det.Positions = Det;%clear Det;

Geo.r_rd = sqrt( ...
    ((Geo.X(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(1,:)'*ones(1,Geo.N_vox)).^2 + ...
    ((Geo.Y(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(2,:)'*ones(1,Geo.N_vox)).^2);
for f = 1:length(Data.w)
    u = exp(1j*(Data.w(f)/Data.ca)*Geo.r_rd)./Geo.r_rd;
    Recon.W0([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
end
% 
% %% Some geometrical precalculations
% Geo.Det.Rd = sqrt(Geo.Det.Positions(1,:).^2+Geo.Det.Positions(2,:).^2);
% Geo.R0 = Geo.Det.Rd-Geo.R;
% for k = 1 : Geo.Det.Number
%     Geo.Det.IND0{k} = max(round(Geo.N_steps*(Geo.r_rd(k,:)-Geo.R0(k))/(2*Geo.R)),1);
% end
% Geo.Det.Mapping = zeros(Geo.Det.Number, Geo.N_vox);
% for k = 1 : Geo.Det.Number
%     Geo.Det.Mapping(k,:) =Geo.Det.IND0{k}+(k-1)*Geo.N_steps;
% end
% 
% %% Ramp filter
% n = size(Geo.X,1);
% [x,y] = meshgrid(1:n,1:n);
% L1 = sqrt((x-1).^2+(y-1).^2);L2 = sqrt((x-1).^2+(y-n).^2);L3 = sqrt((x-n).^2+(y-1).^2);L4 = sqrt((x-n).^2+(y-n).^2);
% LL = min(min(min(L1,L2),L3),L4);
% Geo.LL = 0.858*sqrt(2)*LL / norm(LL);
