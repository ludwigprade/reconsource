function [Recon, Geo] = Model_based_fd(Recon, Geo, Data, REFINE_RADIUS)
tic
%% Scanning for different radii
Norm = []; % This vector accumulates reconstruction error vs. different radii
Solutions = [];
con = 1;
Radii = Geo.Det.Rad0; % Scannig and optimizing over 4 mm
if REFINE_RADIUS
    Radii = Geo.Det.Rad0+(-5:1:5)*1e-4;
end
for Rad = Radii % reconstructing over different radii around the initial guess. Makes sense only when all frequencies are used, otherwise set to Rad0.
    % Building the weight matrix
    % inversion
    if REFINE_RADIUS
        Geo.Det.Rad0 = Rad;
        disp(['Calculating radius: ',num2str(Rad)]);
        Build_weighmatrix
    end
    if isfield(Geo,'Rat')
%         Q = reshape(Data.P, Geo.N_ang, Data.N_f);
%         Q = Q(1:Geo.Rat:end,:);
%         DataP = Q(1:end);
%         Q = reshape(1:length(Data.P), Geo.N_ang, Data.N_f);
%         Q = Q(1:Geo.Rat:end,:);
%         W0  = Recon.W0(Q(1:end),:);
%         
%         RHS = [real(DataP)';imag(DataP)'];
%         
%         Weight.W = [real(W0);imag(W0)];
    else
        Weight.W = [real(Recon.W0);imag(Recon.W0)];
        RHS = [real(Data.P);imag(Data.P)];
    end
    Weight.n_voxels = Geo.N_vox;
    recon.lambdas = getlambdas(Weight.W);
    recon.Linv = speye(Weight.n_voxels,Weight.n_voxels); 
    disp('Starting xfmt-lsqr');
    recon.reconneg = xfmt_lsqr_hybrid(Weight.W, recon.Linv, RHS, recon.lambdas, 50);
%    [x, f0, t] = nnls_conjgrad_armijo(Weight.W, RHS, zeros(Geo.N_vox,1), 0.001, 50, 5); 
                                    %(A,b,x,zetaRel,nIter,cgIter)
                                    %r = b-A*x;
                                    %performing NNLS with the projected conjugate gradient method
                                    %Input: x--initial guess of the solution (usually set as vector 0)
                                    %       zetaRel--relative infinity norm of the projected gradient,used as stopping criteria (e.g. 0.01)
                                    %       nIter--max. number of outer iteration
                                    %       cgIter--max. number of inner iteration (usually set below 5, depending on the problem size)
                                    %Output: x--solution
                                    %        f0--vector containing the value ||Ax-b||^2 after each outer iteration
                                    %        t-- vector containing the execution time after each outer iteration
                                    %terminates if nIter or zetaRel is reached or t>10 min.

   % recon.reconneg =x;
    recon.recon = max(recon.reconneg, 0);
   % recon.recon = recon.reconneg;
    disp('Finishing lsqr');
   
    if REFINE_RADIUS
        in = 100;
    else
        %[recon.L_Curve.NorResidue, recon.L_Curve.NorVector] = L_corner_p(Weight.W, recon, RHS, 1);
        %in = 100;
    end
    in = 120;
    x = (recon.recon(:,in));
    Recon.recon = recon;
    %in = 80; % Setting lambda to a fixed value. L curve is not needed.
    %x = (recon.recon(:,in));
    Norm = [Norm norm(Weight.W*recon.recon(:,in)-reshape(RHS,size(RHS,1)*size(RHS,2),1))];
    Solutions{con} = x;
    con = con + 1;
end
Recon.Solutions=Solutions;
%% Picking the best radius
[~,index] = min(Norm);
if REFINE_RADIUS
    figure();
    plot(Radii, Norm);hold on;plot(Radii(index), Norm(index),'r*');xlabel('Radius');ylabel('Residual norm')
end
Rad = Radii(index);
x = Solutions{index};
Geo.Rad0 = Rad;
Recon.xmb = x;
Recon.Norm=Norm;
Recon.Radii=Radii;
I = 0*Geo.X;
I(Geo.ind) = x;
Recon.I_mb = I;
clear Weight
clear RHS
toc