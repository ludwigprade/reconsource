function N = WeightMatrixNorm(Weight)
N = 0;
for proj = 1 : length(Weight.G_sd)
    G_md = Weight.G_md{proj}';
    for src = 1 : size(Weight.G_sd{proj},2)
        
        G_sd = Weight.G_sd{proj}(:,src);
        G_sm = Weight.G_sm{proj}(:,src);
        w = (ones(size(G_sd,1),1)*G_sm' .* G_md ./ (G_sd*ones(1,size(G_sm,1))));
        w(isinf(w) | isnan(w)) = 0;
        N = N + norm(w, 'fro')^2;
    end
end
N = sqrt(N);