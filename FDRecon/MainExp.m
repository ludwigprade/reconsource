%%  SIMULATION PART
clear all
%close all
format long

%a=load('..\..\MeasurementFiles\IQ-Tomography\ClearAgarTimeSeries_2015.1.26_18.31.7.424.mat');
%a=load('..\..\MeasurementFiles\IQ-Tomography\ClearAgarRecentered_2015.1.27_14.17.8.921.mat');
%a=load('..\..\MeasurementFiles\IQ-Tomography\ClearAgarRecentered_2015.1.27_14.17.8.921.mat');

%a=load('..\..\MeasurementFiles\IQ-Tomography\DiffAgarHexKey05OD_2015.1.30_11.56.11.209.mat');
 %Geo.Det.Rad0 =0.0303;
 %p_out=a.Results.Response;

% a=load('..\..\MeasurementFiles\ClearAgarHexKey_2015.1.29_11.19.48.937.mat');
% Geo.Det.Rad0 =0.0305;

%a=load('..\..\MeasurementFiles\ClearAgarHex1OD_2015.1.29_17.5.23.127.mat');
%Geo.Det.Rad0 =0.0305;

% a=load('..\..\MeasurementFiles\ClearAgarHexKey_2015.1.28_14.34.40.378.mat');
% Geo.Det.Rad0 =0.0312;

% a=load('..\..\MeasurementFiles\IQ-Tomography\ClearAgarRodsCorrected_2014.12.18_17.17.33.053.mat');
% Geo.Det.Rad0 =0.029960000000000;
% r=a.Results.Response;
% p_out=zeros(21,180);
% tInt=0.1;
% SamplingRate=40e3;
% for f=1:21
%    for phi=1:180
%        p_out(f,phi)=mean(r(f,phi,1:SamplingRate*tInt));
%    end
% end
% disp('converting data done');

% %main_forward;
% Geo.Det.Rad0=3e-3;
% a.Results.Response = p_out;

%a=load('..\..\MeasurementFiles\IQ-Tomography\ClearAgarCentered_2014.12.19_12.29.50.301.mat');



% a=load('..\..\MeasurementFiles\IQ-Tomography\ClearAgar_400kss_0.01_2015.1.21_17.6.42.658.mat');
% Geo.Det.Rad0 =0.03060000000000;
% Data.p=transpose(a.Results.Response);

load('..\..\MeasurementFiles\IQ-Tomography\RotTransDoubleRod_2015.3.25_17.0.6.258.mat');
Geo.Det.Rad0=0.003097000000000;
p_out=zeros(length(Results.FreqSpec), length(Results.Angles));
for f=1:length(Results.FreqSpec)
    for a=1:length(Results.Angles)
        p_out(f,a)=Results.Response(f,a)/abs(Results.Reference(f,a));
    end
end
p_out=Results.Response;

Data.F0=a.Results.FreqSpec;
Data.ca =1480;
Geo.R = 3e-3;%  Half of the side length of the square in the middle
Geo.Resolution = 50e-6;

Geo.N_steps = 1*(round(Geo.R*2/Geo.Resolution));
[Geo.X, Geo.Y] = meshgrid(linspace(-Geo.R, Geo.R,Geo.N_steps+1),linspace(-Geo.R, Geo.R,Geo.N_steps+1));
Geo.ind = find(Geo.X.^2+Geo.Y.^2 < Geo.R.^2);
Data.BW = max(Data.F0)-min(Data.F0);
Data.w = 2*pi*Data.F0;
Data.N_f = length(Data.w);
Geo.N_vox = numel(Geo.ind);
       % Data.alpha = (2*Geo.R*Data.delta_f)/Data.ca;
I1 = 0*Geo.X;I1(Geo.ind) = 1;Data.Mask = I1;

% Data.P=a.Results.Response;
Data.p=transpose(p_out);
Data.P=Data.p;


Data.delta_f = min(diff(Data.F0));
Recon.Used_Fs = 1:length(Data.F0);
Data.N_f = length(Data.F0);

Geo.Det.Number = size(Data.p,1);
Geo.Det.Angles = linspace(0,360-20,Geo.Det.Number-10);
theta = Geo.Det.Angles*pi/180;
Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
Geo.Det.Number = size(Det,2);
Geo.Det.Positions = Det;%clear Det;

Recon.W0=BuildWeightMatrix(Data, Geo);
Q = GetImpulseResponseFunc(Data);

[Recon, Geo] = Model_based_fd(Recon, Geo, Data,0); 
Data.alpha = (2*Geo.R*Data.delta_f)/Data.ca;
%tic; [Recon, Data,Geo] = K_space_fd_exp(Recon, Geo, Data,0); toc
Display_results(gca,Recon.I_mb,Geo, Data);
%title(['Integration time: ', num2str(tInt*1000), ' ms']);
title(['Radius: ', num2str(Recon.Radii*1000), 'mm, Voxel: ', num2str(Geo.Resolution), ' mm']);
