function I = Display_results(ax, x, Geo, Data, in, Recon)
axes(ax)
opengl software

if nargin == 6
    recon = Recon.recon;
    x = (recon.recon(:,in));
    I = 0*Geo.X;
    I(Geo.ind) = x;
else
    
    if length(size(x)) == 1
        I = 0*Geo.X;
        I(:) = x;
    else
        I = x;
    end
end
scale=(-Geo.R:Geo.Resolution:Geo.R)*1e3;
%I = I .* imerode(I0 > 0,strel('disk',1)); % Minimual eroding to get rid of boundary artifacts
%I0 = I0 .* imerode(I0 > 0,strel('disk',1)); % Minimual eroding to get rid of boundary artifacts
I=I/max(max(I));
h = imagesc(scale,scale,I);%[-Geo.R Geo.R]*1000,[-Geo.R Geo.R]*1000,
set(gca,'FontSize',30);
set(h,'AlphaData',Data.Mask);
b=colorbar;
zlab = get(b,'ylabel'); 
set(zlab,'String','Optoacoustic Response (a.u.)'); 
set(zlab, 'fontsize', 30);
set(b,'fontsize',30);
hold on
axis equal;
axis tight;
xlabel('mm')
ylabel('mm')

% 
% max1=0;
% xmax1=0;
% ymax1=0;
% for x=1:length(scale)
%     for y=1:length(scale)
%         if I(y,x)>max1
%             max1=I(x,y);
%             xmax1=x;
%             ymax1=y;
%         end
%     end
% end
% 
% max2=0;
% xmax2=0;
% ymax2=0;
% for x=1:length(scale)
%     for y=90:length(scale)
%         if I(y,x)>max2
%             max2=I(y,x);
%             xmax2=x;
%             ymax2=y;
%         end
%     end
% end
% 
% m=(ymax2-ymax1)/(xmax2-xmax1);
% b=ymax2-m*xmax2;
% 
% MaxLine=0;
% for x=1:length(scale)
%     for y=1:length(scale)
%         %if (ymax2-y)/(xmax2-x) == (ymax2-ymax1)/(xmax2-xmax1)
%         if y == m*x + b
%             MaxLine(end+1)=I(y,x);
%         end
%     end
% end
% length(MaxLine)
% figure;
% set(gca,'FontSize',30);
% plot(scale,I(167,:));
% xlabel('x Position (mm)');
% ylabel('Absorption (arb)');
% 
% 
% fit=ezfit(scale, I(167,:), 'a_1*exp(-(x-x_1)^2/(2*s_1^2));a_1=1; x_1=1.4; s_1=0.1');
% showfit(fit);