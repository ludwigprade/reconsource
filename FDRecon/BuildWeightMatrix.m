function W0 = BulidWeightMatrix(Data, Geo)
    %BulidWeightMatrix Summary of this function goes here
    %   Detailed explanation goes here
    %% Building the weight matrix
    tic;
    W0 = zeros(Geo.Det.Number*Data.N_f,Geo.N_vox);
    Geo.Det.Number = size(Data.p,1);
    Geo.Det.Angles = linspace(0,359,Geo.Det.Number);
    theta = Geo.Det.Angles*pi/180;
    Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
    Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
    Geo.Det.Number = size(Det,2);
    Geo.Det.Positions = Det;%clear Det;

    Geo.r_rd = sqrt( ...
        ((Geo.X(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(1,:)'*ones(1,Geo.N_vox)).^2 + ...
        ((Geo.Y(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(2,:)'*ones(1,Geo.N_vox)).^2);
    for f = 1:length(Data.w)
        u = exp(1j*(Data.w(f)/Data.ca)*Geo.r_rd)./Geo.r_rd;
        W0([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
    end
    disp(['Weight matrix calc time: ', num2str(toc)]);
end

