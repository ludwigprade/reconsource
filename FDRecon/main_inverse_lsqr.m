
P = P11;


%% Simulation: inverse
Weight.W = [real(W0);imag(W0)];
RHS = [real(P);imag(P)];
Weight.n_voxels = N_vox;
Recon.lambdas = getlambdas(Weight.W);
Recon.Linv = speye(Weight.n_voxels,Weight.n_voxels); %

Recon.reconneg = xfmt_lsqr_hybrid(Weight.W, Recon.Linv, RHS, Recon.lambdas, 50);
Recon.recon = max(Recon.reconneg, 0);
[Recon.L_Curve.NorResidue, Recon.L_Curve.NorVector] = L_corner_p(Weight.W, Recon, RHS, LFLAG);

%% Plotting the results
h = figure;
I = 0*X;
I(ind) = Mu;
imagesc(I);
I = 0*X;
I(ind) = 1;
I0 = I;
% 
% subplot(223)
in = LineCurve(Recon,LFLAG);
% title('L curve (black) and its curvature (red). Corner marked.')
subplot(211)
x = Recon.recon(:,in);
I = 0*X;
I(ind) = x;
I = I .* imerode(I0 > 0,strel('disk',3));
imagesc([-R R]*1000,[-R R]*1000,interp2(I));
hold on
I1 = 0*X;
I1(ind) = 1;
%plotbounds(I1,'c',1)
axis equal;
axis tight;
xlabel('mm')
ylabel('mm')
axis on
title(['angles = ' num2str(N_ang) ' , res = ' num2str(round(L*1e6)) ' \mum', ', bandwidth: ' num2str(fstart), ' to ' num2str(fend) 'MHz'])
subplot(212)
I = 0*X;
I(ind) = Mu;
imagesc(I);hold on
plotbounds(I0,'c',1)
axis equal;
axis tight;
axis off
colormap jet
% text(20,30,'0.8','fontsize',18,'color','w')
% text(70,30,'0.5','fontsize',18,'color','w')
% text(45,45,'0.1','fontsize',18,'color','w')
% text(50,90,'0.25','fontsize',18,'color','w')
% text(75,70,'0.02','fontsize',18,'color','w')
% text(15,70,'0.05','fontsize',18,'color','w')
title('Real Image');
% subplot(221)
% plot(real(P));hold on;plot(real(P11),'r-');
% title(['snr = ' num2str(snr(P, P11)) ' . Red: real, blue: noisy measurement.'])
maximize
axis tight

