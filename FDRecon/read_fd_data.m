function p = read_fd_data(DataFile, NoiseFile)
if strcmp(DataFile,'D:\Projects\FD\Kspace\Data\13032014\cplx_values_xy.mat')
    load(DataFile)
    p = (p_compl(:,:,1)+p_compl(:,:,2)*1i)./(laser_compl(:,:,1)+laser_compl(:,:,2)*1i);
    %Rad = 293e-4;
    return
end

% 
% load(DataFile)
% pdata = p;
% %load(NoiseFile)
% %pnoise = p;
% p = pdata;
load(DataFile)
p = p_compl./laser_compl;