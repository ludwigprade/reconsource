function s = snr(x,y)
s = 20*log10(norm(y,'fro')/norm(x-y,'fro'));