%%  SIMULATION PART
%clear all
%close all

%a=load('D:\Matlab\RodScan_10112014_1606.mat');
a=load('CarbonRods_121114_1849.mat');
Data.p =transpose(a.Results.Response);

Data.F0 = linspace(0.5e6,7.5e6,21);
Data.delta_f = min(diff(Data.F0));
Recon.Used_Fs = [1:length(Data.F0)];

Data.ca = 1500;
Geo.R = 0.003;%  Half of the side length of the square in the middle

Data.N_f = length(Data.F0);
Geo.Resolution = 10e-6;

%% Building detector array
Geo.Det.Rad0 = 0.02637;%Geo.Len^2/Geo.Resolution;%sqrt(2)*Geo.Len*5; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
Geo.Det.Number = size(Data.p,1);
Geo.Det.Angles = linspace(0,359,Geo.Det.Number);
theta = Geo.Det.Angles*pi/180;
Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
Geo.Det.Number = size(Det,2);
Geo.Det.Positions = Det;%clear Det;
%% Building the weight matrix

Build_weighmatrix
Data.P = [];
Q = Data.p;

getimpulseresponse;Q = (ones(size(Data.p,1),1)*(wind./(0.0005+wind.^2))).*Q;
for nf = Recon.Used_Fs
    Data.P = [Data.P;Q(:,nf)];
end
%% Inverting using kspace and model-based
[Recon, Geo] = Model_based_fd(Recon, Geo, Data,0); 
Display_results(gca,Recon.I_mb,Geo, Data);
