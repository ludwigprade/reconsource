function [Recon, Geo] = Model_based_fd(Recon, Geo, Data, REFINE_RADIUS)
tic
%% Scanning for different radii
Norm = []; % This vector accumulates reconstruction error vs. different radii
Solutions = [];
con = 1;
Radii = Geo.Det.Rad0; % Scannig and optimizing over 4 mm
if REFINE_RADIUS
    Radii = Geo.Rad0+[-2:0.25:2]*1e-3;
end
for Rad = Radii % reconstructing over different radii around the initial guess. Makes sense only when all frequencies are used, otherwise set to Rad0.
    % Building the weight matrix
    % inversion
    if REFINE_RADIUS
        Build_weighmatrix
    end
    if isfield(Geo,'Rat')
        Q = reshape(Data.P, Geo.N_ang, Data.N_f);
        Q = Q(1:Geo.Rat:end,:);
        DataP = Q(1:end);
        Q = reshape(1:length(Data.P), Geo.N_ang, Data.N_f);
        Q = Q(1:Geo.Rat:end,:);
        W0  = Recon.W0i(Q(1:end),:);
        
        RHS = [real(DataP)';imag(DataP)'];
        
        Weight.W = [real(W0);imag(W0)];
    else
        Weight.W = [real(Recon.W0i);imag(Recon.W0i)];
        RHS = [real(Data.P);imag(Data.P)];
    end
    Weight.n_voxels = Geo.N_voxi;
    recon.lambdas = getlambdas(Weight.W);
    recon.Linv = speye(Weight.n_voxels,Weight.n_voxels); %
    recon.reconneg = xfmt_lsqr_hybrid(Weight.W, recon.Linv, RHS, recon.lambdas, 50);
    recon.recon = max(recon.reconneg, 0);
    
    if REFINE_RADIUS
        in = 100;
    else
       % [recon.L_Curve.NorResidue, recon.L_Curve.NorVector] = L_corner_p(Weight.W, recon, RHS, 1);
        in = 100;
    end
    in = 120;
    x = (recon.recon(:,in));
    Recon.recon = recon;
    %in = 80; % Setting lambda to a fixed value. L curve is not needed.
    %x = (recon.recon(:,in));
    Norm = [Norm norm(Weight.W*recon.recon(:,in)-RHS)];
    Solutions{con} = x;
    con = con + 1;
end
%% Picking the best radius
[~,index] = min(Norm);
if REFINE_RADIUS
    figure(10)
    plot(Radii, Norm);hold on;plot(Radii(index), Norm(index),'r*');xlabel('Radius');ylabel('Residual norm')
end
Rad = Radii(index);
x = Solutions{index};
Geo.Rad0 = Rad;
Recon.xmb = x;
I = 0*Geo.Xi;
I(Geo.indi) = x;
Recon.I_mb = I;
clear Weight
clear RHS
toc