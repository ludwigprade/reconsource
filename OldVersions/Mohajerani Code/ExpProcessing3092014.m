%%  SIMULATION PART
clear all
close all
load('D:\Projects\FD\Data\3092014\Z1.mat');
%ref = ref_670_z(:,:,2)+1i*ref_670_z(:,:,1);oas = oas_670_z(:,:,2)+1i*oas_670_z(:,:,1);
ref = ref_670_z(:,:,1)+1i*ref_670_z(:,:,2);oas = oas_670_z(:,:,1)+1i*oas_670_z(:,:,2);
%ref = ref_850_z(:,:,1)+1i*ref_850_z(:,:,2);oas = oas_850_z(:,:,1)+1i*oas_850_z(:,:,2);
%ref = ref_850_z(:,:,2)+1i*ref_850_z(:,:,1);oas = oas_850_z(:,:,2)+1i*oas_850_z(:,:,1);
Data.p =oas./ref;

Data.delta_f = 0.17e6;
Data.F0 = linspace(1e6,4.4e6,21);
Data.w = Data.F0 *2*pi;
Recon.Used_Fs = [1:length(Data.F0)];

Data.ca = 1500*[1 1];
Geo.R = 0.006;%  Half of the side length of the square in the middle

Data.N_f = length(Data.F0);
Geo.Resolution = 130e-6*[1 1];

%% Building detector array
Geo.Det.Rad0 = 0.0273;%Geo.Len^2/Geo.Resolution;%sqrt(2)*Geo.Len*5; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
Geo.Det.Number = 180;
Geo.Det.Angles = linspace(0,359,Geo.Det.Number);
theta = Geo.Det.Angles*pi/180;
Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
Geo.Det.Number = size(Det,2);
Geo.Det.Positions = Det;%clear Det;
Geo.Det.Positionsf = Det;%clear Det;
Geo.Det.Positionsi = Det;%clear Det;
Geo.Det.Positionsi1 = Det;%clear Det;
%% Building the weight matrix

Build_weighmatrix
Data.P = [];
Q = Data.p;

%getimpulseresponse;Q = (ones(size(Data.p,1),1)*(wind./(0.0005+wind.^2))).*Q;
for nf = Recon.Used_Fs
    Data.P = [Data.P;Q(:,nf)];
end
%% Inverting using kspace and model-based
[Recon, Geo] = Model_based_fd(Recon, Geo, Data,0); 
Display_results(gca,Recon.I_mb,Geo, Data);
