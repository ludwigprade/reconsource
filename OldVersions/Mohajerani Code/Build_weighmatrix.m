%% Initializations
if ~isfield(Geo,'X') % The very first run
    % Geo.N_steps = Data.N_f;% 1*(round(Geo.R*2/Geo.Resolution));
    %[Geo.X, Geo.Y] = meshgrid(linspace(-Geo.R, Geo.R,Geo.N_steps+1),linspace(-Geo.R, Geo.R,Geo.N_steps+1));
    [Geo.Xf, Geo.Yf] = meshgrid(-Geo.R:Geo.Resolution(1):Geo.R,-Geo.R:Geo.Resolution(1):Geo.R);
    Geo.indf = find(Geo.Xf.^2+Geo.Yf.^2 < Geo.R.^2);
    Geo.N_voxf = numel(Geo.indf);
    Recon.W0f = zeros(Geo.Det.Number*Data.N_f,Geo.N_voxf);
    I1 = 0*Geo.Xf;I1(Geo.indf) = 1;Data.Maskf = I1;
    
    
    [Geo.Xi, Geo.Yi] = meshgrid(-Geo.R:Geo.Resolution(2):Geo.R,-Geo.R:Geo.Resolution(2):Geo.R);
    Geo.N_steps = size(Geo.Xi,1);
    Geo.indi = find(Geo.Xi.^2+Geo.Yi.^2 < Geo.R.^2);
    Geo.N_voxi = numel(Geo.indi);
    Recon.W0i = zeros(Geo.Det.Number*Data.N_f,Geo.N_voxi);
    I1 = 0*Geo.Xi;I1(Geo.indi) = 1;Data.Maski = I1;
end

%% Building the weight matrix
Geo.r_rdf = sqrt( ...
    ((Geo.Xf(Geo.indf)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsf(1,:)'*ones(1,Geo.N_voxf)).^2 + ...
    ((Geo.Yf(Geo.indf)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsf(2,:)'*ones(1,Geo.N_voxf)).^2);
for f = 1:length(Data.w)
    u = exp(1j*(Data.w(f)/Data.ca(1))*Geo.r_rdf)./Geo.r_rdf;
    Recon.W0f([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
end

Geo.r_rdi = sqrt( ...
    ((Geo.Xi(Geo.indi)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsi(1,:)'*ones(1,Geo.N_voxi)).^2 + ...
    ((Geo.Yi(Geo.indi)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsi(2,:)'*ones(1,Geo.N_voxi)).^2);

Geo.r_rdi1 = sqrt( ...
    ((Geo.Xi(Geo.indi)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsi1(1,:)'*ones(1,Geo.N_voxi)).^2 + ...
    ((Geo.Yi(Geo.indi)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsi1(2,:)'*ones(1,Geo.N_voxi)).^2);

phasenoise = 0*(2*rand(size(Geo.r_rdi))-1)*1*pi;
for f = 1:length(Data.w)
    u = exp(1j*(Data.w(f)/(Data.ca(2)))*Geo.r_rdi+1j*phasenoise)./Geo.r_rdi;
    Recon.W0i([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
end

%% Some geometrical precalculations
Geo.Det.Rd = sqrt(Geo.Det.Positionsi(1,:).^2+Geo.Det.Positionsi(2,:).^2);
Geo.R0 = Geo.Det.Rd-Geo.R;
for k = 1 : Geo.Det.Number
    Geo.Det.IND0{k} = max(round(Geo.N_steps*(Geo.r_rdi(k,:)-Geo.R0(k))/(2*Geo.R)),1);
end
Geo.Det.Mapping = zeros(Geo.Det.Number, Geo.N_voxi);
for k = 1 : Geo.Det.Number
    Geo.Det.Mapping(k,:) = Geo.Det.IND0{k}+(k-1)*Geo.N_steps;
end
if Geo.Det.Number == 1 % Getting the true profile, just for debuggnig and display purposes
    Geo.X0F = (-Geo.R:Geo.Resolution(1):Geo.R);
    for k = 1 : Geo.Det.Number
        Geo.Det.IND0F{k} = max(round(length(Geo.X0F)*(Geo.r_rdf(k,:)-Geo.R0(k))/(2*Geo.R)),1);
    end

    Geo.Det.MappingF = zeros(Geo.Det.Number, Geo.N_voxf);
    for k = 1 : Geo.Det.Number
        Geo.Det.MappingF(k,:) =Geo.Det.IND0F{k}+(k-1)*length(Geo.X0F);
    end
end

%% Ramp filter
n = size(Geo.Xi,1);
[x,y] = meshgrid(1:n,1:n);
L1 = sqrt((x-1).^2+(y-1).^2);L2 = sqrt((x-1).^2+(y-n).^2);L3 = sqrt((x-n).^2+(y-1).^2);L4 = sqrt((x-n).^2+(y-n).^2);
LL = min(min(min(L1,L2),L3),L4);
Geo.LL = 0.858*sqrt(2)*LL / norm(LL);
