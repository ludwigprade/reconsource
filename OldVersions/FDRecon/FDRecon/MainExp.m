%%  SIMULATION PART
clear all
close all

a=load('D:\Matlab\MeasurementFiles\ClearAgarCentered_2014.12.19_12.29.50.301.mat');
Data.p=transpose(a.Results.Response);
Data.F0 = linspace(0.5e6,7.5e6,21);
Geo.Det.Rad0 =0.0304;

% a=load('D:\Matlab\RodScan_10112014_1606.mat');
% Data.p=transpose(a.Response);
% Data.F0 = linspace(0.5e6,7.5e6,21);
% Geo.Det.Rad0 =0.0292;

% a=load('D:\Matlab\SingleRod_2014.12.18_11.50.21.834.mat');
% Data.p=transpose(a.Results.Response);
% Data.p=Data.p(1:170,:);
% Data.F0 = linspace(0.1e6,5e6,21);
% Geo.Det.Rad0 =0.0287;

%a=load('D:\Matlab\SingleRodCorrected_2014.12.18_15.57.11.935.mat');


% a=load('D:\Matlab\ClearAgarCarbonRods_2014.12.17_17.29.20.86.mat');
% Data.p=transpose(a.Results.Response);
% Data.F0 = linspace(0.1e6,5e6,21);

%a=load('CarbonRods_121114_1849.mat');
%a=load('ClearAgar_2014.11.13_14.50.57.559.mat');
%a=load('D:\Matlab\LargeScanCarbonRods_2014.11.14_13.13.36.62.mat');
%a=load('D:\Matlab\ClearAgarCarbonRods_2014.12.17_13.39.7.897.mat');


% a=load('D:\Matlab\ClearAgarRodsCorrected_2014.12.18_17.17.33.053.mat');
% p_in=a.Results.Response;
% p_out=zeros(length(p_in(:,1,1)), length(p_in(1,:,1)));
% nPoints=4000;
% 
% for f=1:length(p_in(:,1,1));
%     for phi=1:length(p_in(1,:,1));
%         p_out(f,phi)=mean(p_in(f,phi, 1:nPoints));
%     end
% end
% Data.p=transpose(p_out);
% disp('Converting data done');

% Data.F0 = linspace(0.5e6,7.5e6,21);
% Geo.Det.Rad0 =0.0302; 

% x=load('D:\Matlab\RodMatrix.mat');
% Data.p=(x.p_out);

%Data.F0=a.Results.FreqSpec;
Data.delta_f = min(diff(Data.F0));
Recon.Used_Fs = 1:length(Data.F0);

%Data.ca = 1.5283e+03;
Data.ca =1480;
Geo.R = 3e-3;%  Half of the side length of the square in the middle

Data.N_f = length(Data.F0);
Geo.Resolution = 20e-6;

%% Building detector array
%Geo.Det.Rad0 = 0.05;%Geo.Len^2/Geo.Resolution;%sqrt(2)*Geo.Len*5; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution

Geo.Det.Number = size(Data.p,1);
Geo.Det.Angles = linspace(0,360-20,Geo.Det.Number-10);
theta = Geo.Det.Angles*pi/180;
%Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
Geo.Det.Number = size(Det,2);
Geo.Det.Positions = Det;%clear Det;
%% Building the weight matrix

Build_weighmatrix
Data.P = [];
Q = Data.p;

%getimpulseresponse;Q = (ones(size(Data.p,1),1)*(wind./(0.05+wind.^2))).*Q;
for nf = Recon.Used_Fs
   Data.P = [Data.P;Q(:,nf)];
end
%% Inverting using kspace and model-based
[Recon, Geo] = Model_based_fd(Recon, Geo, Data,0); 
figure
Display_results(gca,Recon.I_mb,Geo, Data);