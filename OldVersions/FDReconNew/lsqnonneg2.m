function x = lsqnonneg2(C,d, tol, N_iter,G, CpC, FLAG_NNLS)
tol = 0.01;
N_iter = 50;
n = size(C,2);
% Initialize vector of n zeros and Infs (to be used later)
nZeros = zeros(n,1);
wz = nZeros;

% Initialize set of non-active columns to null
P = false(n,1);
% Initialize set of active columns to all and the initial point to zeros
Z = true(n,1);
x = nZeros;
Cpd = C'*d;
resid = d - C*x;
w = C'*resid;

% Set up iteration criterion
outeriter = 0;
iter = 0;
itmax = n*3;
exitflag = 1;

% Outer loop to put variables into set to hold positive coefficients
%KT = G*d;
while any(Z) && any(w(Z) > tol) && outeriter < N_iter % POUYAN
%for any(Z) && any(w(Z) > tol) && outeriter < N_iter % POUYAN
%for outeriter = 1: N_iter % POUYAN
    outeriter = outeriter + 1;
    %if any(Z) && any(w(Z) > tol)
    %    break
    %end
    % Reset intermediate solution z
    z = nZeros;
    % Create wz, a Lagrange multiplier vector of variables in the zero set.
    % wz must have the same size as w to preserve the correct indices, so
    % set multipliers to -Inf for variables outside of the zero set.
    wz(P) = -Inf;
    wz(Z) = w(Z);
    % Find variable with largest Lagrange multiplier
    [~,t] = max(wz);
    % Move variable t from zero set to positive set
    P(t) = true;
    Z(t) = false;
    % Compute intermediate solution using only variables in positive set
    if ~FLAG_NNLS
        z(P) = C(:,P)\d;
    else
     %   z(P) = KT(P);
    end
    %OtherWay
    % inner loop to remove elements from the positive set which no longer belong
    
    while any(z(P) <= 0)
        iter = iter + 1;
        % Find indices where intermediate solution z is approximately negative
        Q = (z <= 0) & P;
        % Choose new x subject to keeping new x nonnegative
        alpha = min(x(Q)./(x(Q) - z(Q)));
        x = x + alpha*(z - x);
        % Reset Z and P given intermediate values of x
        Z = ((abs(x) < tol) & P) | Z;
        P = ~Z;
        z = nZeros;           % Reset z
        z(P) = C(:,P)\d;      % Re-solve for z
    end
    x = z;
    %resid = d - C*x;
    w = Cpd-CpC*x;
end


%--------------------------------------------------------------------------
function options = deprecateX0(options,numInputs,varargin)
% Code to check if user has passed in x0. If so, ignore it and warn of its
% deprecation. Also check whether the options have been passed in either
% the 3rd or 4th input.
if numInputs == 4
    % 4 inputs given; the 3rd (variable name "options") will be interpreted
    % as x0, and the 4th as options
    if ~isempty(options)
        % x0 is non-empty
        warning(message('MATLAB:lsqnonneg:ignoringX0'));
    end
    % Take the 4th argument as the options
    options = varargin{1};
elseif numInputs == 3
    % Check if a non-empty or non-struct has been passed in for options
    % If so, assume that it's an attempt to pass x0
    if ~isstruct(options) && ~isempty(options)
        warning(message('MATLAB:lsqnonneg:ignoringX0'));
        % No options passed, set to empty
        options = [];
    end
end
