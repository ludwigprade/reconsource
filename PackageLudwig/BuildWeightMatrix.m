function Stage=BuildWeightMatrix(Setup)
tic; disp('Building weight matrix');
    Stage.N_steps = 1*(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
    [Stage.X, Stage.Y] = meshgrid(linspace(-Setup.R, Setup.R,Stage.N_steps),linspace(-Setup.R, Setup.R,Stage.N_steps)); %Grid of voxels

    %Stage.ind = find(Stage.X.^2+Stage.Y.^2 < Setup.R.^2); %voxels inside recon-area
    %Stage.N_vox = numel(Stage.ind); %number of voxels inside recon-area
    Stage.N_vox=Stage.N_steps^2;
    %creating detector-ring
    Stage.DetPositions = [Setup.DetRad*cos(Setup.DetAng);Setup.DetRad*sin(Setup.DetAng)];%clear Det;
    Stage.r_rd = sqrt( ...
        ((Stage.X(:)*ones(1,Setup.DetNumber))' - Stage.DetPositions(1,:)'*ones(1,Stage.N_vox)).^2 + ...
        ((Stage.Y(:)*ones(1,Setup.DetNumber))' - Stage.DetPositions(2,:)'*ones(1,Stage.N_vox)).^2); %distance between each voxel and each detector position

    %Building weight matrix
%     W0 = zeros(Setup.DetNumber*Setup.nFreq,Stage.N_vox);
%         for f = 1:Setup.nFreq
%             u = exp(1j*(Setup.w(f)/Setup.ca)*Stage.r_rd)./Stage.r_rd;
%             W0((1:Setup.DetNumber)+(f-1)*Setup.DetNumber,:) = -1j*Setup.w(f)*u;%
%         end
%     Stage.W = [real(W0);imag(W0)];
    
    Stage.DetRd = sqrt(Stage.DetPositions(1,:).^2+Stage.DetPositions(2,:).^2);
    
    Stage.R0 = Stage.DetRd -Setup.R;

    for k = 1 : Setup.DetNumber
        Stage.IND0{k} = max(round(Stage.N_steps*(Stage.r_rd(k,:)-Stage.R0(k))/(2*Setup.R)),1);
    end
    Stage.Mapping = zeros(Setup.DetNumber, Stage.N_vox);
    for k = 1 : Setup.DetNumber
        Stage.Mapping(k,:) = Stage.IND0{k}+(k-1)*Stage.N_steps;
    end
    if Setup.DetNumber == 1 % Getting the true profile, just for debuggnig and display purposes
        Stage.X0F = (-Setup.R:Setup.Resolution:Stage.R);
        for k = 1 : Stage.DetNumber
            Stage.IND0F{k} = max(round(length(Stage.X0F)*(Stage.r_rd(k,:)-Stage.R0(k))/(2*Setup.R)),1);
        end

        Stage.MappingF = zeros(Setup.DetNumber, Stage.N_vox);
        for k = 1 : Setup.DetNumber
            Stage.MappingF(k,:) =Stage.IND0F{k}+(k-1)*length(Stage.X0F);
        end
    end

    %% Ramp filter
    n = size(Stage.X,1);
    [x,y] = meshgrid(1:n,1:n);
    L1 = sqrt((x-1).^2+(y-1).^2);L2 = sqrt((x-1).^2+(y-n).^2);L3 = sqrt((x-n).^2+(y-1).^2);L4 = sqrt((x-n).^2+(y-n).^2);
    LL = min(min(min(L1,L2),L3),L4);
    Stage.LL = 0.858*sqrt(2)*LL / norm(LL);
    disp(['Done in: ', num2str(toc)]);
end