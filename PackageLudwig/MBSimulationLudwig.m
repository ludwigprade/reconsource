function [Setup, RHS] = MBSimulation(Setup)
    MUMAP='HEX';
    tic;disp('Starting simulating data');
    Stage.N_steps = (round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
    Stage.N_vox=Stage.N_steps^2;

    DistanceMatrix=zeros(Stage.N_steps);
    r_rd=zeros(Setup.DetNumber, Stage.N_vox);
    Ruler=linspace(-Setup.R, Setup.R,Stage.N_steps);
    Stage.DetPositions = [Setup.DetRad*cos((2*pi/180)*Setup.DetAng);Setup.DetRad*sin((2*pi/180)*Setup.DetAng)];%clear Det;
    for p=1:Setup.DetNumber
        for x=1:Stage.N_steps
            for y=1:Stage.N_steps
                DistanceMatrix(x,y)=sqrt((Stage.DetPositions(1,p)-Ruler(x))^2+(Stage.DetPositions(2,p)-Ruler(y))^2);
            end
        end
        r_rd(p,:)=DistanceMatrix(:);
    end
    toc;
    clear DistanceMatrix;
    clear Ruler;
    
    W0 = zeros(Setup.DetNumber*Setup.nFreq,Stage.N_vox);
    k=2*pi*Setup.FreqSpec./Setup.ca; %=2*pi/lambda with 1/lambda=f/c
    for f = 1:Setup.nFreq
        W0((1:Setup.DetNumber)+(f-1)*Setup.DetNumber,:)=-1j*Setup.ca*k(f)*exp(j*k(f)*r_rd)./r_rd;
    end     
    clear k;
    clear r_rd;
    
    Recon.SNR = 0; % The intended SNR
    switch MUMAP;
        case 'RODS'
            [Stage.X, Stage.Y] = meshgrid(linspace(-Setup.R, Setup.R,Stage.N_steps),linspace(-Setup.R, Setup.R,Stage.N_steps)); %Grid of voxels
            Objects = [0.5e-3 0 0.1e-3 1;
               % -1e-3 0.5e-3 0.5e-3 1;
              %  +0.000+Setup.Resolution(2)*0 -0.003 .0001 1;
                %+0.000 -0.003 .0003 1;
                %0.005 -0.004 .0006 1.5;
             %   -Setup.R*0.69 +Setup.R*0.69 .0002 1;
                %+0.000 0.000 .0003 1;
                %-Setup.R*0.56 +Setup.R*0.69 .0002 1;
                ]; % Soviele Objekte wie du willst: [position_x position_y radius absorption]
            Mu = ones(numel(Stage.X),1)*0.0;

            %OBJECTS = [];
            for k = 1 : size(Objects,1)
                P0 = Objects(k,[1 2]);
                if k == -1
                    ind0 = find((Stage.X-P0(1)).^2+(Stage.Y-P0(2)).^2 < Objects(k,3).^2 & (Stage.X-P0(1)).^2+(Stage.Y-P0(2)).^2 > (Objects(k,3)*0.7).^2);
                else
                    ind0 = find((Stage.X-P0(1)).^2+(Stage.Y-P0(2)).^2 < Objects(k,3).^2);
                end
                Mu(ind0) = Objects(k,4);
            end
            clear Stage.X
            clear Stage.Y
        case 'HEX'
            MuSquare=zeros(Stage.N_steps);
            theta = 0:60:360;
            SideLength=0.75e-3;
            xv = (SideLength/Setup.Resolution)*cosd(theta)+Stage.N_steps/2;
            yv = (SideLength/Setup.Resolution)*sind(theta)+Stage.N_steps/2;
            for x=1:Stage.N_steps
                for y=1:Stage.N_steps
                   MuSquare(x,y)=0.1*inpolygon(x,y,xv,yv);
                end
            end
           % imagesc(MuSquare)
            Mu=[];
            for x=1:Stage.N_steps   
                Mu= [Mu;MuSquare(:,x)];
            end
    end
    %d0 = Geo.R*0.9;
%    Mu = Mu(Stage.ind);
    P = W0*Mu;
    imagesc(abs(reshape(P, Setup.DetNumber, Setup.nFreq)));
    Data.noise = randn(size(Data.P_nonoise))+1j*randn(size(Data.P_nonoise));
    Data.coef = (10^(-Recon.SNR/20)*norm(Data.P_nonoise))/norm(Data.noise);
    Data.P = Data.P_nonoise+Data.coef*Data.noise;
    %snr(Data.P, Data.P_nonoise)
    I3 = 0*Stage.X;
%    I3(Stage.ind) = Mu;
    Recon.Mu = Mu;
    Recon.TrueImage = I3;
    Setup.P=Data.P;
    RHS = [real(Data.P);imag(Data.P)];
    %figure(31);h31 = imagesc(I3);axis equal;axis tight
    disp(['Done in: ', num2str(toc)]);
    Setup.Mu=I3;
    Setup.PMatrix=reshape(Setup.P, Setup.DetNumber, Setup.nFreq);
    
end

