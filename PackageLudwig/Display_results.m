function Display_results(I, Setup)

    %creating mask
    %N_steps = 1*(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
    N_steps=length(I);
    [X, Y] = meshgrid(linspace(-Setup.R, Setup.R,N_steps),linspace(-Setup.R, Setup.R,N_steps)); %Grid of voxels

    ind = find(X.^2+Y.^2 < Setup.R.^2); %voxels inside recon-area
    N_vox = numel(ind); %number of voxels inside recon-area
    Mask = 0*X; %setting all voxels to zero
    Mask(ind) = 1; %set those to one inside the recon-area

    scale=-Setup.R:Setup.Resolution:Setup.R;
    NormFactor=max(max(I));
    h=imagesc(scale*1000, scale*1000, I/NormFactor);%[-Geo.R Geo.R]*1000,[-Geo.R Geo.R]*1000,
    set(gca,'FontSize',16);
    set(h,'AlphaData',Mask);
    title(['Resolution: ', num2str(Setup.Resolution*1e6), ' um, ', ' DetRad: ', num2str(Setup.DetRad*1e3),' mm']);
    colorbar
    hold on
    axis equal;
    axis tight;
    xlabel('mm')
    ylabel('mm')
end