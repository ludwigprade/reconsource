%clear all
close all
format long
addpath(genpath('D:\Matlab'));
DATATYPE='SIMDATA';
switch DATATYPE
    case 'REALDATA'
        disp('loading data');
        tic;
        %[RHS,Setup]=LoadMeasurement('D:\MeasurementFiles\IQ-Tomography\RotTransHex60f_2015.3.25_18.24.4.109.mat');
        %START OF functional set of parameters, do not change!!!
        %[RHS,Setup]=LoadMeasurement('D:\MeasurementFiles\IQ-Tomography\RotTransDoubleRod_2015.3.25_17.0.6.258.mat');
        [RHS,Setup]=LoadMeasurement('/media/ludwigprade/Daten/MeasurementFiles/IQ-Tomography/RotTransDoubleRod_2015.3.25_17.0.6.258.mat');
        Setup.ca = 1485; % Speed of sound
        Setup.Resolution = 30e-6; % Inverse resolution
        Setup.R = 10e-3;% Reconstruction radius. Half of the side length of the square in the middle
        Setup.DetRad = 0.03729; %Detector ring radius   
        %END OF functional set of parameters, do not change!!!  


    %      Setup.ca = 1485; % Speed of sound
    %      Setup.Resolution = 40e-6; % Inverse resolution
    %      Setup.R = 5e-3;% Reconstruction radius. Half of the side length of the square in the middle
    %      Setup.DetRad = 0.03733; %Detector ring radius   


    %     [RHS,Setup]=LoadMeasurement('..\..\MeasurementFiles\IQ-Tomography\ClearaAgarCentered_2015.1.20_16.37.26.2.mat');
    %     Setup.ca = 1485; % Speed of sound
    %     Setup.Resolution = 40e-6; % Inverse resolution
    %     Setup.R = 2e-3;% Reconstruction radius. Half of the side length of the square in the middle
    %     Setup.DetRad = 0.03041; %Detector ring radius

    %     %START OF functional set of parameters, do not change!!!
    %     [RHS, Setup]=LoadMeasurement('CarbonRods_121114_1849');
    %     %Recon parameters
    %     Setup.ca = 1488.5; % Speed of sound
    %     Setup.Resolution = 40e-6; % Inverse resolution
    %     Setup.R = 3e-3;% Reconstruction radius. Half of the side length of the square in the middle
    %     Setup.DetRad = 0.03015; %Detector ring radius
    %     %END OF functional set of parameters, do not change!!!


        disp(['Data loaded in: ', num2str(toc), ' sec']);
    case 'TDDATA'
        Setup.nFreq=60;
        %Setup.R = 5e-3;
        %Setup.ca = 1488.5; % Speed of sound
        Setup.FreqSpec=linspace(0.5,10,Setup.nFreq)*1e6;
        %[RHS, Setup]=LoadTDMeasurement(Setup, 'D:\MeasurementFiles\TD_Hexkey\sigMat_ph_ludwig.mat');
        Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
        Setup.w = 2*pi*Setup.FreqSpec;
        Setup.FreqSpec=linspace(0.5,5,Setup.nFreq)*1e6;
        Setup.DetAng=linspace(0,360*(1-1/Setup.DetNumber),Setup.DetNumber);
        [RHS,Setup] = LoadTDSimulation(Setup);
    case 'SIMDATA'
        %Make up some data for the simulation
    %     Setup.ca = 1485; % Speed of sound
    %     Setup.DetRad = 0.033; %Detector ring radius

        Setup.ca = 1500; % Speed of sound
        Setup.DetRad = 40e-3; %Detector ring radius

        Setup.Resolution = 20e-6; % Inverse resolution
        Setup.R =4e-3;% Reconstruction radius. Half of the side length of the square in the middle
        Setup.nFreq=20;
        Setup.fStart=1e6;
        Setup.fStop=25e6;
        Setup.FreqSpec=linspace(Setup.fStart,Setup.fStop,Setup.nFreq);
        Setup.DetNumber=180;
        Setup.DetAng=linspace(0,360*(1-1/Setup.DetNumber),Setup.DetNumber);
        Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
        Setup.w = 2*pi*Setup.FreqSpec;
        
        Stage.N_steps = (round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
        Stage.N_vox=Stage.N_steps^2;
        Stage.DetPositions = [Setup.DetRad*cos((pi/180)*Setup.DetAng);Setup.DetRad*sin((pi/180)*Setup.DetAng)];%clear Det;
        
        Mu   = BuildPhantom('HEX', Setup, Stage);
        
        [Setup_td, RHS_td] = TDSimulation(Setup, Mu);
        [Setup_fd, RHS_fd] = FDMBSimulation(Setup, Stage ,Mu);
end
if isfield(Setup, 'Mu');
    figure;Display_results(Setup.Mu, Setup);
end
Setup.R=1e-3;
Setup.Resolution = 50e-6;

I = MBRecon(RHS, Setup, Stage);

r_rd = BuildRMatrix(Setup, Stage);
W0   = BuildWeightMatrixLudwig(r_rd, Setup, Stage);

lambdas=[0 logspace(-6,1,200)*norm(W0,'fro')];
Linv = speye(Stage.N_vox ,Stage.N_vox ); %

I_vec = xfmt_lsqr_hybrid(W0, Linv, RHS_fd,lambdas(120), 50); %Recon.lambda
