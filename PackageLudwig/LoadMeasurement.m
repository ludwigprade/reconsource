function [RHS,Setup] = LoadMeasurement( FileName )
    load(FileName);
    
    Setup.FreqSpec=Results.FreqSpec;
    Setup.nFreq=length(Results.FreqSpec);
    Setup.DetAng=Results.Angles*pi/180;
    Setup.DetNumber=length(Results.Angles);
    PMatrix=zeros(Setup.nFreq, Setup.DetNumber);
    Setup.BW = max(Setup.FreqSpec)-min(Setup.FreqSpec);
    Setup.w = 2*pi*Setup.FreqSpec;
    
    %ImpulseResponse=Q(ones(size(Setup.P,1),1)*(wind./(0.05+wind.^2))).*Q;
    
    if ndims(Results.Response)==3
        disp('time series found');
        tInt=Results.Setup.SweepTime;
        SamplingRate=Results.Setup.SamplingRate;
        for phi=1:Setup.DetNumber
            for f=1:Setup.nFreq
                if isfield(Results, 'Reference');
                    disp('Reference found');
                    PMatrix(f,phi)=mean(Results.Response(f,phi,1:SamplingRate*tInt))/mean(Results.Reference(f,phi,1:SamplingRate*tInt));
                else
                    PMatrix(f,phi)=mean(Results.Response(f,phi,1:SamplingRate*tInt));
                end
            end
        end
    else
        disp('no time series found');
        if isfield(Results, 'Reference');
            disp('Reference found');
            for phi=1:Setup.DetNumber
                for f=1:Setup.nFreq
                    PMatrix(f,phi)=Results.Response(f,phi)/Results.Reference(f,phi);
                end
            end
            PMatrix=transpose(PMatrix);
        else
            PMatrix=transpose(Results.Response);
        end
        %Data.p =transpose(Results.Response);
    end
   
    PMatrix=GetImpulseResponse(Setup, PMatrix);
    
    Setup.P=[];
    for f=1:Setup.nFreq
        Setup.P = [Setup.P;PMatrix(:,f)];
    end
    %getimpulseresponse;
    %Setup.P  = (ones(size(Setup.P,1),1)*(wind./(0.05+wind.^2))).*Setup.P ;
    RHS = [real(Setup.P);imag(Setup.P)];
   
end

