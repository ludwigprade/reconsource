function varargout = ReconGui(varargin)
% RECONGUI MATLAB code for ReconGui.fig
%      RECONGUI, by itself, creates a new RECONGUI or raises the existing
%      singleton*.
%
%      H = RECONGUI returns the handle to a new RECONGUI or the handle to
%      the existing singleton*.
%
%      RECONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in RECONGUI.M with the given input arguments.
%
%      RECONGUI('Property','Value',...) creates a new RECONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ReconGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ReconGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ReconGui

% Last Modified by GUIDE v2.5 29-Apr-2015 11:57:01

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ReconGui_OpeningFcn, ...
                   'gui_OutputFcn',  @ReconGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ReconGui is made visible.
function ReconGui_OpeningFcn(hObject, eventdata, handles, varargin)

% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ReconGui (see VARARGIN)

% Choose default command line output for ReconGui
handles.output = hObject;
handles.Setup=0;
handles.RHS=0;
handles.DATATYPE='FDDATA';
handles.SHOWSIM='DATA';
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ReconGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ReconGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in BackProjectButton.
function BackProjectButton_Callback(hObject, eventdata, handles)
% hObject    handle to BackProjectButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
guidata(hObject, handles);

handles.Setup.ca         = str2num(get(handles.edit4, 'String')); % m/s Speed of sound
handles.Setup.DetRad     = str2num(get(handles.edit3, 'String'))*1e-3; % mm Detector ring radius  
handles.Setup.Resolution = str2num(get(handles.edit1, 'String'))*1e-6; % �m Inverse resolution
handles.Setup.R          = str2num(get(handles.edit2, 'String'))*1e-3;% mm Reconstruction radius. Half of the side length of the square in the middle 

handles.Stage=BuildWeightMatrix(handles.Setup);
handles.I_bp = BPRecon(handles.Setup, handles.Stage, 0);
cla(handles.axes1);
axes(handles.axes1);
Display_results(handles.I_bp, handles.Setup);
guidata(hObject, handles);



% --- Executes on button press in LoadDataButton.
function LoadDataButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadDataButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
FileName=get(handles.edit5, 'String');
disp('loading data');

switch handles.DATATYPE
    case 'FDDATA'
        disp('Loading FD Data');
        [handles.RHS, handles.Setup]=LoadMeasurement(FileName);
        cla(handles.axes2);
        axes(handles.axes2);
        SigMat=transpose(reshape(handles.Setup.P, handles.Setup.DetNumber, handles.Setup.nFreq));
        imagesc(handles.Setup.DetAng*180/pi,handles.Setup.FreqSpec*1e-6, abs(SigMat));
        xlabel('Projection Angle (�)');
        ylabel('Frequeny (Mhz)');
        
    case 'TDDATA'
        toc;
        disp('Loading TD Data');
        handles.Setup.nFreq=60;
        handles.Setup.R = 5e-3;
        handles.Setup.ca = 1488.5; % Speed of sound
        handles.Setup.FreqSpec=linspace(0.5,5,handles.Setup.nFreq)*1e6;
        [handles.RHS, handles.Setup]=LoadTDMeasurement(handles.Setup, 'D:\MeasurementFiles\TD_Hexkey\sigMat_ph_ludwig.mat');
        handles.Setup.BW = max(handles.Setup.FreqSpec)-min(handles.Setup.FreqSpec);
        handles.Setup.w = 2*pi*handles.Setup.FreqSpec;
        handles.Setup.FreqSpec=linspace(0.5,5,handles.Setup.nFreq)*1e6;
        handles.Setup.DetAng=linspace(0,360*(1-1/handles.Setup.DetNumber),handles.Setup.DetNumber);
        disp(['Data loaded in: ' , num2str(toc)]);
        
        cla(handles.axes2);
        axes(handles.axes2);
        SigMat=transpose(reshape(handles.Setup.P, handles.Setup.DetNumber, handles.Setup.nFreq));
        imagesc(handles.Setup.DetAng*180/pi,handles.Setup.FreqSpec*1e-6, abs(SigMat));
        xlabel('Projection Angle (�)');
        ylabel('Frequeny (Mhz)');

    case 'SIMDATA'
        disp('Simulating Data');
        handles.Setup.ca =         str2num(get(handles.CaSimEdit, 'String')); % Speed of sound
        handles.Setup.DetRad =     str2num(get(handles.DetRadSimEdit, 'String'))*1e-3; %Detector ring radius
        handles.Setup.Resolution = str2num(get(handles.ResSimEdit, 'String'))*1e-6; % Inverse resolution
        handles.Setup.R =          str2num(get(handles.ReconRadSimEdit, 'String'))*1e-3;% Reconstruction radius. Half of the side length of the square in the middle
        handles.Setup.nFreq=       str2num(get(handles.NoFreqEdit, 'String'));
        handles.Setup.DetNumber=   str2num(get(handles.NoProjEdit, 'String'));
        handles.Setup.StartFreq=   str2num(get(handles.StartFreqEdit, 'String'))*1e6;
        handles.Setup.StopFreq=    str2num(get(handles.StopFreqEdit, 'String'))*1e6;
        
        handles.Setup.FreqSpec=linspace(handles.Setup.StartFreq,handles.Setup.StopFreq,handles.Setup.nFreq);
        handles.Setup.DetAng=linspace(0,360*(1-1/handles.Setup.DetNumber),handles.Setup.DetNumber);
        handles.Setup.BW = max(handles.Setup.FreqSpec)-min(handles.Setup.FreqSpec);
        handles.Setup.w = 2*pi*handles.Setup.FreqSpec;
        
        [handles.Setup, handles.RHS] =MBSimulation(handles.Setup);


        switch handles.SHOWSIM
            case 'PHANTOM';
                cla(handles.axes2);
                axes(handles.axes2);
                Display_results(handles.Setup.Mu, handles.Setup);
            case 'DATA'
                cla(handles.axes2);
                axes(handles.axes2);
                SigMat=transpose(reshape(handles.Setup.P, handles.Setup.DetNumber, handles.Setup.nFreq));
                imagesc(handles.Setup.DetAng*180/pi,handles.Setup.FreqSpec*1e-6, abs(SigMat));
                xlabel('Projection Angle (�)');
                ylabel('Frequeny (Mhz)');
        end  
        
end

guidata(hObject, handles);


% --- Executes on button press in FDDataButton.
function FDDataButton_Callback(hObject, eventdata, handles)
% hObject    handle to FDDataButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.DATATYPE='FDDATA';
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of FDDataButton


% --- Executes on button press in radiobutton2.
function radiobutton2_Callback(hObject, eventdata, handles)
% hObject    handle to radiobutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.DATATYPE='TDDATA';
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of radiobutton2


% --- Executes on button press in SimulationButton.
function SimulationButton_Callback(hObject, eventdata, handles)
% hObject    handle to SimulationButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.DATATYPE='SIMDATA';
guidata(hObject, handles);
% Hint: get(hObject,'Value') returns toggle state of SimulationButton



function ResSimEdit_Callback(hObject, eventdata, handles)
% hObject    handle to ResSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ResSimEdit as text
%        str2double(get(hObject,'String')) returns contents of ResSimEdit as a double


% --- Executes during object creation, after setting all properties.
function ResSimEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ResSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function ReconRadSimEdit_Callback(hObject, eventdata, handles)
% hObject    handle to ReconRadSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ReconRadSimEdit as text
%        str2double(get(hObject,'String')) returns contents of ReconRadSimEdit as a double


% --- Executes during object creation, after setting all properties.
function ReconRadSimEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ReconRadSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DetRadSimEdit_Callback(hObject, eventdata, handles)
% hObject    handle to DetRadSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DetRadSimEdit as text
%        str2double(get(hObject,'String')) returns contents of DetRadSimEdit as a double


% --- Executes during object creation, after setting all properties.
function DetRadSimEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DetRadSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function CaSimEdit_Callback(hObject, eventdata, handles)
% hObject    handle to CaSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of CaSimEdit as text
%        str2double(get(hObject,'String')) returns contents of CaSimEdit as a double


% --- Executes during object creation, after setting all properties.
function CaSimEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to CaSimEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StartFreqEdit_Callback(hObject, eventdata, handles)
% hObject    handle to StartFreqEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StartFreqEdit as text
%        str2double(get(hObject,'String')) returns contents of StartFreqEdit as a double


% --- Executes during object creation, after setting all properties.
function StartFreqEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StartFreqEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function StopFreqEdit_Callback(hObject, eventdata, handles)
% hObject    handle to StopFreqEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StopFreqEdit as text
%        str2double(get(hObject,'String')) returns contents of StopFreqEdit as a double


% --- Executes during object creation, after setting all properties.
function StopFreqEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StopFreqEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function NoFreqEdit_Callback(hObject, eventdata, handles)
% hObject    handle to NoFreqEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoFreqEdit as text
%        str2double(get(hObject,'String')) returns contents of NoFreqEdit as a double


% --- Executes during object creation, after setting all properties.
function NoFreqEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoFreqEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function NoProjEdit_Callback(hObject, eventdata, handles)
% hObject    handle to NoProjEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoProjEdit as text
%        str2double(get(hObject,'String')) returns contents of NoProjEdit as a double


% --- Executes during object creation, after setting all properties.
function NoProjEdit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoProjEdit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ShowDataButton.
function ShowDataButton_Callback(hObject, eventdata, handles)
    handles.DATATYPE='FDDATA';
    cla(handles.axes2);
    axes(handles.axes2);
    if isfield(handles.Setup, 'P')
        SigMat=transpose(reshape(handles.Setup.P, handles.Setup.DetNumber, handles.Setup.nFreq));
        imagesc(handles.Setup.DetAng*180/pi,handles.Setup.FreqSpec*1e-6, abs(SigMat));
        xlabel('Projection Angle (�)');
        ylabel('Frequeny (Mhz)');
    end
    guidata(hObject, handles);
% hObject    handle to ShowDataButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ShowDataButton


% --- Executes on button press in ShowPhantomButton.
function ShowPhantomButton_Callback(hObject, eventdata, handles)
    handles.DATATYPE='PHANTOM';
    cla(handles.axes2);
    axes(handles.axes2);    
    if isfield(handles.Setup, 'Mu')
        Display_results(handles.Setup.Mu, handles.Setup);
    end
    guidata(hObject, handles);
% hObject    handle to ShowPhantomButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ShowPhantomButton
