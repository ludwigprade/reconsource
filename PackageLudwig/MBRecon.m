function I = MBRecon(RHS, Setup, Stage)
%model based inversion
tic; disp('Starting model based inversion');
lambdas=[0 logspace(-6,1,200)*norm(Stage.W,'fro')];
Linv = speye(Stage.N_vox ,Stage.N_vox ); %

I_vec = xfmt_lsqr_hybrid(Stage.W, Linv, RHS,lambdas(120), 50); %Recon.lambda

%[x, f0, t] = nnls_conjgrad_armijo(W, RHS, zeros(Stage.N_vox,1), 0.001, 50, 5); 
I_vec = max(I_vec, 0);
%Norm = norm(W*I_vec-reshape(RHS,size(RHS,1)*size(RHS,2),1)); %calculating norm of matrix, because its fun
I = zeros(Stage.N_steps+1, Stage.N_steps+1);

I(Stage.ind) = I_vec; %rearranging absorbtion vector to nice matrix
clear I_vec;
clear ReconResults;
clear lambdas;
clear Linv;
disp(['Done in: ', num2str(toc)]);
end

