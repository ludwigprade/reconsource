function I=BPRecon(Setup, Stage, NN_FLAG)
% function [Recon, Data, Geo] = BPRecon(Recon, Geo, Data, NN_FLAG)
tic; disp('Starting back-projection inversion');
N_steps = 1*(round(Setup.R*2/Setup.Resolution)); %number of voxels in each direction
N_f=length(Setup.FreqSpec);
delta_f=Setup.FreqSpec(2)-Setup.FreqSpec(1);
DetPositions = [Setup.DetRad*cos(Setup.DetAng);Setup.DetRad*sin(Setup.DetAng)];%clear Det;
R0 = sqrt(DetPositions(1,:).^2+DetPositions(2,:).^2)-Setup.R;
alpha = (2*Setup.R*delta_f)/Setup.ca;

d0 = 2*Setup.R/(N_steps); % effective resolution. It is also Data.ca/(2 N_f delta_f)
f0 = Setup.FreqSpec(1);
pl = exp(-1j*2*pi*f0*(0:N_steps-1)*d0/Setup.ca);
for k = 1 : Setup.DetNumber
    ql(k,:) = exp(1j*(R0(k)+d0)*Setup.w/Setup.ca);%*exp(-1j*2*pi*f0*d0/Data.ca(2));
    Rn(k,:) = R0(k)+(1:N_steps)*d0;%-Geo.d0/2;
end

M = zeros(N_f,N_steps);
for m = 1 : N_f
    
    M(m,:) = exp(-1j*2*pi*(m-1)*alpha*(0:N_steps-1)/(N_steps));
end

Data.pl = pl;
M = M*diag(pl);
Recon.M = M;

%%
Q = reshape(-1j*conj(Setup.P), Setup.DetNumber, N_f);
Q = Q ./(ones(Setup.DetNumber,1)*Setup.FreqSpec*2*pi);
Q = Q .* ql;
if 1
    if alpha == 1
        q2 = 1;
    else
        q2 = 10;
    end
    q1 = round(alpha*q2);
    Recon.qs = [q1 q2];
    Tem = zeros(Setup.DetNumber,q2*N_steps);
    Tem(:,1:q1:q1*(N_f-1)+1) = Q; % For phase only do ./abs(Q)
    g0 = ifft(Tem,[],2);
    Dat = 2*real(g0(:,1:N_steps)*diag(1./pl))*q2*N_steps;

end
Dat = max(Dat,0).*Rn/Setup.DetNumber;
Dat = Dat*(Setup.Resolution/Setup.Resolution)^2;
%%
Recon.Dat = Dat;
Datp = Dat';

if Setup.DetNumber == 1
    solution = Datp(Stage.Mapping);
else
    solution = sum(Datp(Stage.Mapping),1);
end
I = NaN(size(Stage.X));
sig = solution;
I(Stage.ind) = sig;
I3 = I;
I(isnan(I)) = mean(I(~isnan(I)));
F = fft2(I);
if Setup.DetNumber > 1
    F = F(:).*Stage.LL(:);
end
K = (max(0,real(ifft2(reshape(F,size(Stage.X,1),size(Stage.X,1))))));
%%
K(isnan(I3)) = NaN;

if ~NN_FLAG
    Recon.I_FFT = K;
else
    Recon.I_NN = K;
end
I=K;
disp(['Done in: ', num2str(toc)]);
return
%% Plots for debugging
if 0
    figure
    t = SH;
    subplot(311)
    plot(Vec(:,t));hold on;plot(Mec*Dat(t,:)','r-');
    subplot(312)
    plot(abs(Q(t,:)));hold on;plot(abs(M*Dat(t,:)'),'r-');
    subplot(313)
    plot(Dat(t,:));
end

