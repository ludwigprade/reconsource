function PMatrix=GetImpulseResponse(Setup, PMatrix)
disp('Correcting for frequency response');
%     im = imread('ir.jpg');
%     im = (rgb2gray(im));
%     close
%     imagesc(imrotate(im,-3));colormap gray
%     im = imrotate(im,-3);
%     im(im == 0) = 255;
%     imagesc(im);colormap gray
%     close
%     im = im(:,131:1246);
%     imagesc(im);colormap gray
%     im = im(1:952,:);
%     imagesc(im);colormap gray
%     close
%     [i,j] = find(im < 30);
%     plot(j,i,'.')
%     close
%     plot(j,size(im,1)-i,'.')
%     J = 1 : size(im,2);
%     I = 0*J;
%     for k = 1 : size(im,2)
%         I(k) = size(im,1)-mean(i(j == k));
%     end
%     close all
%     plot(J,I,'r.')
%     close all
%     
%     g = round(max(J)*Setup.FreqSpec/7000000);
%     g(g > length(I)) = length(I);
%     wind = I(g);
%     wind = wind / max(wind);
    
    a_1 = 1;
    s_1 = 7.4877e+05;
    x_1 = 3.4451e+06;
    H = (a_1*exp(-(Setup.FreqSpec-x_1).^2/(2*s_1^2))).^(1/2);
    H(H<=0.5)=0.5;
    
    N=0;
    G=1./H;
    %G=(1./H).*(abs(H).^2./(abs(H).^2+N));
    GMat=(ones(size(PMatrix,1),1)*G);
    PMatrix=GMat.*PMatrix;
    %Q=PMatrix;
    %Q=GMat.*Q;
    %Q = (ones(size(PMatrix,1),1)*(wind./(0.00005+wind.^2))).*Q;
    
end