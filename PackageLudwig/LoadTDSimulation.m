function [RHS,Setup] = LoadTDSimulation(Setup)
    FileName='D:\Matlab\TimeDomainSimulation\TD-HexkeySim.mat';
    load(FileName);
    Setup.DetRad = r_sensor;
    Setup.ca=c;
    n = nSamples;
    fs = SampleRate;
    Setup.DetNumber=length(SigMat(1,:));
    PMatrix=zeros(Setup.nFreq, Setup.DetNumber);
    InputFspec=1:fs/length(SigMat(1:end,1)):fs;
    
    for p=1:Setup.DetNumber
        X=fft(SigMat(:,p));
        for f = 1:Setup.nFreq
            fvalue=knnsearch(InputFspec', Setup.FreqSpec(f));
            PMatrix(f,p)=X(fvalue);
            Setup.FreqSpec(f)=InputFspec(fvalue);
        end
    end
    PMatrix=transpose(PMatrix);
    Setup.P=[];
    for f=1:Setup.nFreq
        Setup.P = [Setup.P;PMatrix(:,f)];
    end
    %getimpulseresponse;
    %Setup.P  = (ones(size(Setup.P,1),1)*(wind./(0.05+wind.^2))).*Setup.P ;
    RHS = [real(Setup.P);imag(Setup.P)];
    Setup.PMatrix=PMatrix;
end

