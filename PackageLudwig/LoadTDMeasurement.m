function [RHS,Setup] = LoadTDMeasurement(Setup, FileName )
    load(FileName);
    Setup.DetRad = 34.3e-3;
    n = 200;
    fs = 100e6;
    Setup.DetNumber=length(sigMat(1,:));
    PMatrix=zeros(Setup.nFreq, Setup.DetNumber);
    fspec=1:fs/length(sigMat(200:end,1)):fs;
    
    for p=1:Setup.DetNumber
        X=fft(sigMat(n:end,p));
        for f = 1:Setup.nFreq
            PMatrix(f,p)=X(knnsearch(fspec', Setup.FreqSpec(f)));
            Setup.FreqSpec(f)=fspec(knnsearch(fspec', Setup.FreqSpec(f)));
        end
    end
    PMatrix=transpose(PMatrix);
    Setup.P=[];
    for f=1:Setup.nFreq
        Setup.P = [Setup.P;PMatrix(:,f)];
    end
    %getimpulseresponse;
    %Setup.P  = (ones(size(Setup.P,1),1)*(wind./(0.05+wind.^2))).*Setup.P ;
    RHS = [real(Setup.P);imag(Setup.P)];
end

