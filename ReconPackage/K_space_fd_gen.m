function [Recon, Data, Geo] = K_space_fd_gen(Geo, Data, NN_FLAG)
    disp('FFTBP inversion');
    tic;
    Geo.Det.Number=180;
     %% Some geometrical precalculations
    Geo.Det.Rd = sqrt(Geo.Det.Positions(1,:).^2+Geo.Det.Positions(2,:).^2);
    Geo.R0 = Geo.Det.Rd-Geo.R;
    for k = 1 : Geo.Det.Number
         Geo.Det.IND0{k} = max(round(Geo.N_steps*(Geo.r_rd(k,:)-Geo.R0(k))/(2*Geo.R)),1);
    end
    Geo.Det.Mapping = zeros(Geo.Det.Number, Geo.N_vox);
    for k = 1 : Geo.Det.Number
        Geo.Det.Mapping(k,:) = Geo.Det.IND0{k}+(k-1)*Geo.N_steps;
    end
    
    %% Ramp filter
    n = size(Geo.X,1);
    [x,y] = meshgrid(1:n,1:n);
    L1 = sqrt((x-1).^2+(y-1).^2);
    L2 = sqrt((x-1).^2+(y-n).^2);
    L3 = sqrt((x-n).^2+(y-1).^2);
    L4 = sqrt((x-n).^2+(y-n).^2);
    LL = min(min(min(L1,L2),L3),L4);
    Geo.LL = 0.858*sqrt(2)*LL / norm(LL);
    
    Geo.d0 = 2*Geo.R/(Geo.N_steps); % effective resolution. It is also Data.ca/(2 N_f delta_f)
    f0 = Data.F0(1);
    pl = exp(-1j*2*pi*f0*[0:Geo.N_steps-1]*Geo.d0/Data.ca);
    for k = 1 : Geo.Det.Number
        ql(k,:) = exp(1j*(Geo.R0(k)+Geo.d0)*Data.w/Data.ca);%*exp(-1j*2*pi*f0*Geo.d0/Data.ca(2));
        Rn(k,:) = Geo.R0(k)+[1:Geo.N_steps]*Geo.d0;%-Geo.d0/2;
    end
    Data.alpha = (2*Geo.R*Data.delta_f)/Data.ca;
    M = zeros(Data.N_f,Geo.N_steps);
    for m = 1 : Data.N_f
        M(m,:) = exp(-1j*2*pi*(m-1)*Data.alpha*[0:Geo.N_steps-1]/(Geo.N_steps));
    end
    Data.pl = pl;
    M = M*diag(pl);
    Recon.M = M;

    %%
    
    Q = reshape(-1j*conj(Data.P), Geo.Det.Number, Data.N_f);
    Q = Q ./(ones(Geo.Det.Number,1)*Data.w);
    Q = Q .* ql;
    if 1
        if Data.alpha == 1
            q2 = 1;
        else
            q2 = 10;
        end
        q1 = round(Data.alpha*q2);
        Recon.qs = [q1 q2];
        Tem = zeros(Geo.Det.Number,q2*Geo.N_steps);
        Tem(:,1:q1:q1*(Data.N_f-1)+1) = Q; % For phase only do ./abs(Q)
        g0 = ifft(Tem,[],2);
        Dat = 2*real(g0(:,1:Geo.N_steps)*diag(1./pl))*q2*Geo.N_steps;

    end
    Dat = max(Dat,0).*Rn/Geo.Det.Number;
%    Dat = Dat*(Geo.SimRes/Geo.Resolution)^2;
    %%
    Recon.Dat = Dat;
    Datp = Dat';
    if Geo.Det.Number == 1
        solution = Datp(Geo.Det.Mapping);
    else
        solution = sum(Datp(Geo.Det.Mapping),1);
    end
    I = NaN(size(Geo.X));
    sig = solution;
    I(Geo.ind) = sig;
    I3 = I;
    I(isnan(I)) = mean(I(~isnan(I)));
    F = fft2(I);
    if Geo.Det.Number > 1
        F = F(:).*Geo.LL(:);
    end
    K = (max(0,real(ifft2(reshape(F,size(Geo.X,1),size(Geo.X,1))))));
    %%
    K(isnan(I3)) = NaN;
    if ~NN_FLAG
        Recon.I_FFT = K;
    else
        Recon.I_NN = K;
    end
    disp(['FFTBP in: ' num2str(toc), ' sec']);
    return
    %% Plots for debugging
    if 0
        figure
        t = SH;
        subplot(311)
        plot(Vec(:,t));hold on;plot(Mec*Dat(t,:)','r-');
        subplot(312)
        plot(abs(Q(t,:)));hold on;plot(abs(M*Dat(t,:)'),'r-');
        subplot(313)
        plot(Dat(t,:));
    end
    
end

