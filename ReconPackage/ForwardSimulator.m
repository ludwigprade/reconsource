function [SimResults, Geo, Data, Recon] = ForwardSimulator(Geo, Data)
    disp('Simulating data');
    tic;
    %Data.delta_f = 1*Data.ca/(2*Geo.R);
    %Data.N_f = round(Data.BW/Data.delta_f)+1;%Data.ca(1)/(2*Data.BW)
    %Data.F0 = 0.1e6+[0:Data.N_f-1]*Data.delta_f;
    Data.F0=linspace(Data.Fmin, Data.Fmax, Data.N_f);
    Data.delta_f=Data.F0(2)-Data.F0(1);
    Data.BW = max(Data.F0)-min(Data.F0);
    Data.w = 2*pi*Data.F0;
    %Data.N_f = length(Data.w);
    

    [Geo, Data] = SetStage(Geo, Data);
    [ Recon, Geo, Data ] = WeightMatrixBuilder(Geo, Data);
    Recon.SNR = Inf; % The intended SNR
    Objects = [+0.003 -0.002 .00010 1;
        +0.003 0.002 .00010 1;
      %  +0.000+Geo.Resolution(2)*0 -0.003 .0001 1;
        %+0.000 -0.003 .0003 1;
        %0.005 -0.004 .0006 1.5;
     %   -Geo.R*0.69 +Geo.R*0.69 .0002 1;
        %+0.000 0.000 .0003 1;
        %-Geo.R*0.56 +Geo.R*0.69 .0002 1;
        ]; % Soviele Objekte wie du willst: [position_x position_y radius absorption]
    Mu = ones(numel(Geo.X),1)*0.0;

    %OBJECTS = [];
    for k = 1 : size(Objects,1)
        P0 = Objects(k,[1 2]);
        if k == -1
            ind0 = find((Geo.X-P0(1)).^2+(Geo.Y-P0(2)).^2 < Objects(k,3).^2 & (Geo.X-P0(1)).^2+(Geo.Y-P0(2)).^2 > (Objects(k,3)*0.7).^2);
        else
            ind0 = find((Geo.X-P0(1)).^2+(Geo.Y-P0(2)).^2 < Objects(k,3).^2);
        end
        Mu(ind0) = Objects(k,4);
    end
    %d0 = Geo.R*0.9;
    Mu = Mu(Geo.ind);
    Data.P_nonoise = Recon.W0*Mu;
    Data.noise = randn(size(Data.P_nonoise))+1j*randn(size(Data.P_nonoise));
    Data.coef = (10^(-Recon.SNR/20)*norm(Data.P_nonoise))/norm(Data.noise);
    Data.P = Data.P_nonoise+Data.coef*Data.noise;
    %snr(Data.P, Data.P_nonoise)
    I3 = 0*Geo.X;
    I3(Geo.ind) = Mu;
    Recon.Mu = Mu;
    Recon.TrueImage = I3;
    %figure(31);h31 = imagesc(I3);axis equal;axis tight
    
    disp(['Data simulated in: ', num2str(toc), ' sec']);
    SimResults.P_nonoise=Recon.W0*Mu;
    SimResults.P=SimResults.P_nonoise+Data.coef*Data.noise;
    SimResults.Coef= (10^(-Recon.SNR/20)*norm(SimResults.P_nonoise))/norm(Data.noise);
    SimResults.TrueImage=I3;
    SimResults.Mask=Data.Mask;
    
end

