%% Plotting
figure
colormap jet
I = 0*X;
I2 = 0*X;
I2(ind) = 1;

I = 0*X;
I1 = 0*X;
I1(ind) = 1;
Disp = cumsum(Sig);
opengl software
for t = N_ang; % detector angle index
    sig = Disp(t,:);
    I = nan(size(X));
    I(ind) = sig;
    Mask = (imerode(I2,strel('disk',1)));
    I = I .* Mask;
    I(~Mask) = NaN;
    subplot(221)
    h = imagesc([-R R]*1000,[-R R]*1000,I);
    axis equal;axis tight;%xlabel('mm');ylabel('mm');
    set(h,'AlphaData',I1 > 0 & Mask);
    axis equal;axis tight;axis off
    subplot(222)
    imagesc([-round(3e-3/L):round(3e-3/L)]*L,[-round(3e-3/L):round(3e-3/L)]*L,I(round((size(I,1)+1)/2)+[-round(3e-3/L):round(3e-3/L)],round((size(I,1)+1)/2)+[-round(3e-3/L):round(3e-3/L)]))
    axis equal;axis tight;xlabel('mm');ylabel('mm');
    
    drawnow;%delete(h1)
end
maximize
