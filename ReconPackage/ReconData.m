%%  SIMULATION PART

%% Die Messungen im experimentellen Format
%A = reshape(Data.P,Geo.Det.Number,Data.N_f);
%Recon.Used_Fs = 1:Data.N_f; % This one would only work for experimental data

clear all
format long

%Geo.Det.Rad0 = 0.029960000000000; % Detector ring radius, best would be Geo.Len^2/Geo.Resolutio
% Geo.Det.Number = 180;    
% Data.Fmin=500e3;
% Data.Fmax=7.5e6;
% Data.N_f=21;

Data.ca = 1480;
Geo.Resolution = 50e-6; % Inverse resolution
Geo.R = 2e-3;% Reconstruction radius. Half of the side length of the square in the middle

%Load measurement data
%Geo.Det.Rad0 = 0.030050000000; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
%[Geo, Data] = LoadMeasurementFiles(Geo, Data, '..\..\MeasurementFiles\IQ-Tomography\ClearAgarRodsCorrected_2014.12.18_17.17.33.053.mat');



Geo.Det.Rad0 = 0.032500; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
[Geo, Data] = LoadMeasurementFiles(Geo, Data, '..\..\MeasurementFiles\IQ-Tomography\RotTransDoubleRod_2015.3.25_17.0.6.258.mat');
%Forward_Simulation;
%[Geo, Data] = SetStage(Geo, Data);
%[SimResults, Geo, Data, Recon] = ForwardSimulator(Geo, Data);

%Reconstruction
%Geo.R = 0.005;% Reconstruction radius. Half of the side length of the square in the middle
%Geo.Det.Rad0 = 0.07; % Detector ring radius, best would be Geo.Len^2/Geo.Resolution
%Geo.Rad0=0.07;

%% Inverting using kspace and model-based
[Recon_mb, Geo_mb] = Model_based_fd(Geo, Data,0); % if the last flag set to 1, it would refine the intial radius guess
figure;
Display_results(gca,Recon_mb.I,Geo_mb, Data);


% USE FFTBP
[Recon_bp, Data_bp, Geo_bp] = K_space_fd_gen(Geo, Data,0);

%% Plot 
% if exist('h31') & ishandle(h31) 
%     close(31); 
% end
figure
subplot(224)
plot(Geo.X(Geo.ind),Geo.Y(Geo.ind),'r.');hold on;axis off
plot(Geo.Det.Positions(1,:),Geo.Det.Positions(2,:),'bs') % because here inverse means assumed
axis equal;
subplot(222)
Display_results(gca,Recon_bp.I_FFT, Geo_bp, Data_bp);title('FFT')
colorbar
subplot(223)
if isfield(Recon_mb, 'TrueImage')
    h = imagesc(Recon_mb.TrueImage);set(h,'AlphaData',SimResults.Mask);axis equal;axis tight
end
maximize
subplot(221)
figure;
Display_results(gca,Recon_mb.I,Geo_mb, Data);

%Angular_speed = abs(diff(Data.ca))*max(Data.w)*mean(Geo.r_rdi1(:))*180/(pi*Data.ca(1)^2);
return