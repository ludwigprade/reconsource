function [Recon, Geo, Data] = WeightMatrixBuilder(Geo, Data)
    %% Initializations
    disp('Building weight matrix');
    tic;
     
    
    %calculate resulting A and phi 
    Recon.W0 = zeros(Geo.Det.Number*Data.N_f,Geo.N_vox);
    for f = 1:length(Data.w)
        u = exp(1j*(Data.w(f)/Data.ca)*Geo.r_rd)./Geo.r_rd;
        Recon.W0([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
    end
    
    Geo.Det.Number = 180;
    Geo.Det.Angles = linspace(0,359,Geo.Det.Number);
    theta = Geo.Det.Angles*pi/180;
    Geo.Det.Angles = linspace(0,360-1,Geo.Det.Number);
    Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
    Geo.Det.Number = size(Det,2);
    Geo.Det.Positions = Det;%clear Det;

    Geo.r_rd = sqrt( ...
        ((Geo.X(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(1,:)'*ones(1,Geo.N_vox)).^2 + ...
        ((Geo.Y(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(2,:)'*ones(1,Geo.N_vox)).^2);
    for f = 1:length(Data.w)
        u = exp(1j*(Data.w(f)/Data.ca)*Geo.r_rd)./Geo.r_rd;
        Recon.W0([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
    end
    
%     %create inverse weight matrix
%     Geo.Det.Positionsi = Geo.Det.Positionsf;
%     %creating mesh
%     [Geo.Xi, Geo.Yi] = meshgrid(-Geo.R:Geo.Resolution:Geo.R,-Geo.R:Geo.Resolution:Geo.R);
%     Geo.N_steps = size(Geo.Xi,1);
%     Geo.indi = find(Geo.Xi.^2+Geo.Yi.^2 < Geo.R.^2);
%     Geo.N_voxi = numel(Geo.indi);
%    
%     I1 = 0*Geo.Xi;I1(Geo.indi) = 1;Data.Maski = I1;
% 
%     %calculate distance between each voxel and detector position
%     Geo.r_rdi = sqrt( ...
%         ((Geo.Xi(Geo.indi)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsi(1,:)'*ones(1,Geo.N_voxi)).^2 + ...
%         ((Geo.Yi(Geo.indi)*ones(1,Geo.Det.Number))' - Geo.Det.Positionsi(2,:)'*ones(1,Geo.N_voxi)).^2);
%     Recon.W0i = zeros(Geo.Det.Number*Data.N_f,Geo.N_voxi);
%     %calculate resulting A and phi 
%     for f = 1:length(Data.w)
%         u = exp(1j*(Data.w(f)/(Data.ca))*Geo.r_rdi)./Geo.r_rdi;
%         Recon.W0([1:Geo.Det.Number]+(f-1)*Geo.Det.Number,:) = -1j*Data.w(f)*u;%
%     end
    
    disp(['Weight matrix build in: ', num2str(toc), ' sec']);
end

