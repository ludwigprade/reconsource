function lambdas=getlambdas(W)

N = norm(W,'fro');

lambdas=[0 logspace(-6,1,200)*N];
