function [Geo, Data] = LoadMeasurementFiles(Geo, Data, FileName )
    load(FileName);
    nFreq=length(Results.FreqSpec);
    nProj=length(Results.Angles);
    Temp_Var = zeros(size(Results.Response,1),size(Results.Response,2));
    
    if ndims(Results.Response)==3
        r=Results.Response;
        tInt=0.1;
        SamplingRate=40e3;
        for f=1:21
            for phi=1:180
                Temp_Var(f,phi)=mean(r(f,phi,1:SamplingRate*tInt));
            end
        end
    else
        disp('no time series found/n');
        for f=1:nFreq
             for phi=1:nProj
                 Temp_Var(f*phi) = Results.Response(f,phi);
             end
         end
    end
    
%     if isfield(Results,'Reference')
%         for f=1:nFreq
%             for p=1:nProj
%                 if f==1 && p==1
%                     P_result(f*p) = Results.Response(f,p);
%                 else
%                     P_result(f*p)=Results.Response(f,p)/(abs(Results.Reference(1,1))-abs(Results.Reference(f,p)));
%                 end
%             end
%         end
%     else
%         for f=1:nFreq
%             for p=1:nProj
%                 P_result(f*p) = Results.Response(f,p);
%             end
%         end
%     end
Data.P=Temp_Var;
    
Data.F0=Results.FreqSpec;
Geo.N_steps = 1*(round(Geo.R*2/Geo.Resolution));
[Geo.X, Geo.Y] = meshgrid(linspace(-Geo.R, Geo.R,Geo.N_steps+1),linspace(-Geo.R, Geo.R,Geo.N_steps+1));
Geo.ind = find(Geo.X.^2+Geo.Y.^2 < Geo.R.^2);
Data.BW = max(Data.F0)-min(Data.F0);
Data.w = 2*pi*Data.F0;
Data.N_f = length(Data.w);
Geo.N_vox = numel(Geo.ind);
       % Data.alpha = (2*Geo.R*Data.delta_f)/Data.ca;
I1 = 0*Geo.X;I1(Geo.ind) = 1;Data.Mask = I1;


Data.delta_f = min(diff(Data.F0));
Data.N_f = length(Data.F0);

Geo.Det.Number = size(Temp_Var,2);
Geo.Det.Angles = linspace(0,360-20,Geo.Det.Number-10);
theta = Geo.Det.Angles*pi/180;
Det = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];
Geo.Det.Number = size(Det,2);
Geo.Det.Positions = Det;%clear Det;


[Geo, Data] = SetStage(Geo, Data);
end

