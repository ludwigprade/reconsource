function [h] = Display_results(ax, I, Geo, Data, in, Recon)
%axes(ax)
% opengl software
% 
% if nargin == 6
%     recon = Recon.recon;
%     x = (recon.recon(:,in));
%     I = 0*Geo.X;
%     I(Geo.ind) = x;
% else
%     
%     if length(size(x)) == 1
%         I = 0*Geo.X;
%         I(:) = x;
%     else
%         I = x;
%     end
% end
%I = I .* imerode(I0 > 0,strel('disk',1)); % Minimual eroding to get rid of boundary artifacts
%I0 = I0 .* imerode(I0 > 0,strel('disk',1)); % Minimual eroding to get rid of boundary artifacts
scale=-Geo.R:Geo.Resolution:Geo.R;
NormFactor=max(max(I));
h=imagesc(scale*1000, scale*1000, I/NormFactor);%[-Geo.R Geo.R]*1000,[-Geo.R Geo.R]*1000,
set(gca,'FontSize',16);
set(h,'AlphaData',Data.Mask);
title(['Resolution: ', num2str(Geo.Resolution*1e6), ' �m, ', ' DetRad0: ', num2str(Geo.Det.Rad0*1e3),' mm']);
colorbar
hold on
axis equal;
axis tight;
xlabel('mm')
ylabel('mm')
