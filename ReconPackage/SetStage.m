function [Geo, Data] = SetStage(Geo, Data)
    %creating mesh
%     Geo.Det.Angles = [0:Geo.Det.Number-1]*360/Geo.Det.Number;% linspace(0,360-1,Geo.Det.Number);
%     theta = Geo.Det.Angles*pi/180;
%     Geo.Det.Positions = [Geo.Det.Rad0*cos(theta);Geo.Det.Rad0*sin(theta)];%clear Det;
%     [Geo.X, Geo.Y] = meshgrid(-Geo.R:Geo.Resolution:Geo.R,-Geo.R:Geo.Resolution:Geo.R);
%     Geo.N_steps = size(Geo.X,1);
%     Geo.ind = find(Geo.X.^2+Geo.Y.^2 < Geo.R.^2);
%     Geo.N_vox = numel(Geo.ind);


%calculate distance between each voxel and detector position
Geo.r_rd = sqrt( ...
    ((Geo.X(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(1,:)'*ones(1,Geo.N_vox)).^2 + ...
    ((Geo.Y(Geo.ind)*ones(1,Geo.Det.Number))' - Geo.Det.Positions(2,:)'*ones(1,Geo.N_vox)).^2);
end

