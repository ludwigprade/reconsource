function plotbounds(BW, C, W)

[B,L] = bwboundaries(BW,'noholes');
hold on
for k = 1:length(B)
    boundary = B{k};
    h = plot(boundary(:,2), boundary(:,1), C, 'LineWidth', W);
end
